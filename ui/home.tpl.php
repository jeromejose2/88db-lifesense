{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "nav.tpl.php" %}
<div id="cont-wrap">
    <ul id="content_panel">
        <li style="background-image: url('images/homeBanner.jpg');width: 900px;">
        </li>
    </ul>
    <ul id="content_panel">
         <li>
            <div style="">

                <div><h1>{{announce_title}}</h1></div>
                <br>
                <div>{{announce_content}}</div>
            </div>

        </li>
        <li>
            <div class="login_container">
                <h1 class="page-title" style="color: #000000">Login</h1>
                <form id="signin" action="{{root}}signin" method="post">
                    <fieldset>
                        <input type="text" class="text" id="user" name="user" placeholder="Username" maxlength="64"/>
                        <input type="password" class="text" id="pass" name="pass" placeholder="Password" maxlength="32"/>
                        <input type="submit" class="login_button" id="submit" name="submit" value="Sign In"/>
                        <div>
                            <label class="login_keeploggedin">Keep me logged in</label>
                            <input type="checkbox" name="keepmeloggedin" id="keepmeloggedin" value="true"/>
                        </div>
                    </fieldset>
                </form>
                <div>
                    <a href="forgot-password">Forgot your password?</a>
                </div>
            </div>                    
        </li>                
    </ul>  
    <div style="text-align: right;">
        <!--<script type="text/javascript" src="http://counter1.statcounterfree.com/private/counter.js?c=6550641e941c6832d946c260787bb33e"></script>-->
    </div>

</div>
{% endblock content %}