{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "nav.tpl.php" %}
        <div id="cont-wrap">
            <br>
            <br>
            <br>
            <br>
            <div align="center" style="height: 400px">
<h1>Thanks for signing up!</h1>
<br>
<br>
<h3>Kindly check your email now for your temporary password,<br>
then click <a href="{{root}}home">HERE</a> to log in.</h3><br>
<br>
<br>
If you didn't receive an email, please check your SPAM or PROMOTIONS folders.<br>
Make sure to add "help@lifesense.ph" in your email contacts list so you won't miss our latest promos!<br>                
            </div>
        </div>
{% endblock content %}