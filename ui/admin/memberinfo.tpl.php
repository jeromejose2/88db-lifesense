{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Member Information</h1>
    <br>
    <br>
    <div class="order_detail_container">
        {% for item in member_info %}
        <div class="form_item">
            <div class="form_label"><label>ID: </label></div>
            <div><input type="text" class="text" value="{{item.id}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Name: </label></div>
            <div><input type="text" class="text" value="{{item.firstname}} {{item.mi}} {{item.lastname}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Address: </label></div>
            <div><textarea  rows="4" readonly>{{item.street}},{{item.city}},{{item.state}},{{item.country}}</textarea></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Phone No.: </label></div>
            <div><input type="text" class="text" value="{{item.phone_number}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Mobile No.: </label></div>
            <div><input type="text" class="text" value="{{item.mobile_number}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>E-mail: </label></div>
            <div><input type="text" class="text" value="{{item.email}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Gender: </label></div>
            <div><input type="text" class="text" value="{% if item.gender == 1 %}Male{% else %}Female{% endif %}" readonly>
            </div>
        </div>    
        <div class="form_item">
            <div class="form_label"><label>Date of Birth: </label></div>
            <div><input type="text" class="text" value="{{item.dateofbirth}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Tax Id No.: </label></div>
            <div><input type="text" class="text" value="{{item.tin}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Passport No.: </label></div>
            <div><input type="text" class="text" value="{{item.passport}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Driver's License: </label></div>
            <div><input type="text" class="text" value="{{item.drivers_license}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Marital Status: </label></div>
            <div><input type="text" class="text" value="{% if item.marital_status == 1 %}Single{% else %}Married{% endif %}" readonly>
            </div>
        </div>
        <br>
        <div class="form_item">
            <div class="form_label"><label>Name of Spouse: </label></div>
            <div><input type="text" class="text" value="{{item.spouse_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Contact No.: </label></div>
            <div><input type="text" class="text" value="{{item.spouse_contact_no}}" readonly></div>
        </div>
        <br>
        <div class="form_item">
            <div class="form_label"><label>Beneficiaries: </label></div>
            <div>&nbsp;</div>
        </div>
        {% if item.beneficiary1_name %}
        <div class="form_item">
            <div class="form_label"><label>Name: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary1_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Reationship: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary1_relationship}}" readonly></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Contact No.: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary1_contact}}" readonly></div>
        </div>            
        {% endif %}
        {% if item.beneficiary2_name %}
        <div class="form_item">
            <div class="form_label"><label>Name: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary2_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Reationship: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary2_relationship}}" readonly></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Contact No.: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary2_contact}}" readonly></div>
        </div>            
        {% endif %}
            {% if item.beneficiary3_name %}
        <div class="form_item">
            <div class="form_label"><label>Name: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary3_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Reationship: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary3_relationship}}" readonly></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Contact No.: </label></div>
            <div><input type="text" class="text" value="{{item.beneficiary3_contact}}" readonly></div>
        </div>            
        {% endif %}
        <br>
        <div class="form_item">
            <div class="form_label"><label>Office Address: </label></div>
            <div><textarea rows="4" readonly>{{item.office_street}},{{item.office_city}},{{item.office_state}},{{item.office_country}}</textarea></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Shipping Address: </label></div>
            <div><textarea rows="4" readonly>{{item.shipping_street}},{{item.shipping_city}},{{item.shipping_state}},{{item.shipping_country}} {{item.shipping_zipcode}}</textarea></div>
        </div>
        <br>
        <div class="form_item">
            <div class="form_label"><label>Other Contact: </label></div>
            <div>&nbsp;</div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Facebook ID: </label></div>
            <div><input type="text" class="text" value="{{item.facebook_id}}" readonly></div>
        </div>                    
        <div class="form_item">
            <div class="form_label"><label>Skype ID: </label></div>
            <div><input type="text" class="text" value="{{item.skype_id}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Twitter ID: </label></div>
            <div><input type="text" class="text" value="{{item.twitter_id}}" readonly></div>
        </div>
        <br>
        <div class="form_item">
            <div class="form_label"><label>Bank Name: </label></div>
            <div><input type="text" class="text" value="{{item.bank_name}}" readonly></div>
        </div>                    
        <div class="form_item">
            <div class="form_label"><label>Bank Account Name: </label></div>
            <div><input type="text" class="text" value="{{item.bank_account_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Bank Account No.: </label></div>
            <div><input type="text" class="text" value="{{item.bank_account_no}}" readonly></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Bank Branch: </label></div>
            <div><input type="text" class="text" value="{{item.bank_branch}}" readonly></div>
        </div>        
        <br>        
        <div class="form_item">
            <div class="form_label"><label>Sponsor: </label></div>
            <div><input type="text" class="text" value="{% if item.sponsor == 0 %} None {%else %} {{item.sponsor}}{% endif %}" readonly></div>
        </div>                    
        
        {% endfor %}        
    </div>
</div>
{% endblock content %}