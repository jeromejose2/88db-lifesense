{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Update Information</h1>
    <br>
    <br>
    <br>
    <br>    
    <div class="order_detail_container" align="center">
        <br>
        <br>
        <form action="{{root}}admin/account-settings/action/updateinfo" method="post">
            <fieldset>
                <div class="form_item">
                    <div class="form_label"><label>Full Name: </label></div>
                    <div><input type="text" class="text" name="fullname" maxlength="130" value="{{fullname}}"></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>E-mail: </label></div>
                    <div><input type="text" class="text" name="email" maxlength="130" value="{{email}}"></div>
                </div>
                <div class="form_item">
                    <div><button type="submit" class="form_button">Update</button></div>
                </div>
            </fieldset>
        </form>
        <br>
        <br>
    </div>
</div>
{% endblock content %}