{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'admin_nav.tpl.php' %}
<script language="javascript" type="text/javascript">
			
    $(document).on('change', '.quantity', function(){
        var total = 0;
        $('.quantity').each(function() {
            total += parseInt($(this).val());
        });
        $('#sum').val(total)
    });
</script>
		
<div id="cont-wrap">
		<table class="roboto" align = 'center'>				 
			<tr>
                <th style='height:50px;' width=100><center>IMAGE</center></th>
                <th style='height:50px;' width=100><center>NAME</center></th>
                <th style='height:50px;' width=150><center>DESCRIPTION</center></th>
                <th style='height:50px;' width=100><center>PRICE</center></th>
                <th style='height:50px;' width=250><center>ENTER PIN & CODE NUMBER<br><font face="Verdana" size=1 color="red">(required)</font></center></th>
            </tr>
            {% for item in kit %}
            <tr>
    		<td style='overflow:hidden; width:100px;' align='center'><img src="{{root}}images/{{item.image}}" width=100 height=100></td>
				<td style='overflow:hidden; width:100px;' align='center'>{{item.name}}</td>
				<td style='overflow:hidden; width:150px;' align='center'>{{item.desc}}</td>
				<td style='overflow:hidden; width:100px;' align='center'>{{item.price}}</td>
				<td style='overflow:hidden; width:250px;' align='center'>
					<table class = 'hovertable'>
						<tr><td><font face='Verdana' size='2'>PIN #:</td><td><input type='password' name ='pin'></td></tr>
						<tr><td><font face='Verdana' size='2'>CODE #:</td><td><input type='password' name ='code'></td>
					</table>
                </td>
            </tr>
            {% endfor %}
		</table>
</div>    
{% endblock content %}