{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Settings</h1>
    <div class="common_table_container">
        <div>
            <button id="changepass" value="{{root}}admin/account-settings/action/changepass" class="form_button">Change Password</button>
        </div>
        <br>
        <div>
            <button id="updateinfo" value="{{root}}admin/account-settings/action/updateinfo" class="form_button">Update Information</button>
        </div>
        <br>
        <div>
            <button id="updateinfo" value="{{root}}admin/GenerateCode" class="form_button">Generate Activation Codes</button>
        </div>
    </div>
</div>
{% endblock content %}