{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Add New User</h1>
    <br>
    <br>
    <br>
    <div class="order_detail_container" align="center">
        <form action="{{root}}admin/add-user" method="post">
            <fieldset>
                <div class="form_item">
                    <div class="form_label"><label>Username:</label></div>
                    <div><input type="text" class="text" name="user" value="" maxlength="16"/></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Password:</label></div>
                    <div><input type="text" class="text" name="pass" value="" maxlength="32"/></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Role:</label></div>
                    <div><select name="role">
                                    <option value="1">Administrator</option>
                                    <option value="2">Manager</option>
                                    <option value="3">Sales</option>
                                </select></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Full Name:</label></div>
                    <div><input type="text" class="text" name="fullname" value="" maxlength="128"/></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>E-mail:</label></div>
                    <div><input type="text" class="text" name="email" value="" maxlength="64"/></div>
                </div>   
                <div class="form_item">
                    <div class="form_label">&nbsp;</div>
                    <div><button type="submit" name="submit" class="form_button">Submit</button></div>
                </div> 
            </fieldset>
        </form>        
    </div>
</div>
{% endblock content %}