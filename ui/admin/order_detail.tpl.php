{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Order Detail</h1>
     <br>
    <br>
    <br>
    <br>
    <center>
    <div class="order_detail_container">
        {% for item in orders %}
        <form method="post" action="{{root}}admin/orders/action/confirm/orderid/{{item.id}}">
        <div class="form_item">
            <div class="form_label"><label>Order Date: </label></div>
            <div><input type="text" class="text" value="{{item.order_datetime}}" readonly></div>
        </div>

        <div class="form_item">
            <div class="form_label"><label>Order ID: </label></div>
            <div><input type="text" class="text" value="{{item.id}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Customer Name: </label></div>
            <div><input type="text" class="text" value="{{item.customer_name}}" readonly></div>
        </div>
        <div class="form_item_div">
            <div class="normal_label"><label>Billing Information</label></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>First Name: </label></div>
            <div><input type="text" class="text" value="{{item.billing_first_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Last Name: </label></div>
            <div><input type="text" class="text" value="{{item.billing_last_name}}" readonly></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>E-Mail: </label></div>
            <div><input type="text" class="text" value="{{item.billing_email}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Phone: </label></div>
            <div><input type="text" class="text" value="{{item.billing_phone}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Company: </label></div>
            <div><input type="text" class="text" value="{{item.billing_company}}" readonly></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Billing Address: </label></div>
            <div><textarea class="text" rows="4" readonly>{{item.billing_street_addr}},{{item.billing_city}},{{item.billing_state}},{{item.billing_country}} {{item.billing_zip}}</textarea></div>
        </div>        
        <div class="form_item_div">
            <div class="normal_label"><label>Shipping Details</label></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>First Name: </label></div>
            <div><input type="text" class="text" value="{{item.shipping_first_name}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Last Name: </label></div>
            <div><input type="text" class="text" value="{{item.shipping_last_name}}" readonly></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Phone: </label></div>
            <div><input type="text" class="text" value="{{item.shipping_phone}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Company: </label></div>
            <div><input type="text" class="text" value="{{item.shipping_company}}" readonly></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Shipping Address: </label></div>
            <div><textarea class="text" rows="4" readonly>{{item.shipping_street_addr}},{{item.shipping_city}},{{item.shipping_state}},{{item.shipping_country}} {{item.shipping_zip}}</textarea></div>
        </div>        
        {% endfor %}
        <div class="form_item_div">
            <div class="normal_label"><label>Order Items</label></div>
        </div>
        <div class="form_item">
            <div class="form_label">&nbsp;</div>
            <div class="form_itemized_container">
                <div class="order_row">
                    <div class="qty_top">Qty</div>
                    <div class="name_top">Item</div>
                    <div class="price_top">Price</div>
                </div>
                {% for item in order_items %}                
                <div class="order_row">
                    <div class="qty">{{item.quantity}}</div>
                    <div class="name">{{item.name}}</div>
                    <div class="price">&#x20b1; {{item.price}}</div>
                </div>
                {% endfor %}
                <div class="order_row">
                    <div class="qty_last">Total</div>
                    <div class="name_last">&nbsp;</div>
                    <div class="price_last">&#x20b1; {{total}}</div>
                </div>                
            </div>
        </div>
        </form>
    </div>
    </center>    
</div>
{% endblock content %}