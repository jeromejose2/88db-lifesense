{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Members</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">LifeSense Members</span>
            <br>
            <br>
            <div class="toprow">
                <div class="name"><b>Member</b></div>
                <div class="name"><b>Sponsor</b></div>
                <div class="date"><b>Date Joined</b></div>
                <div class="number"><b>Points Earned</b></div>
                <div class="status"><b>Status</b></div>
                <div class="name"><b>Referral Code</b></div>
                <div class="topcolumn"></div>                
            </div>
            {% for item in members %}
            <div class="row">
                <div class="name"><a href="{{root}}admin/memberinfo/mid/{{item.id}}">{{item.name}}</a></div>
                <div class="name">{{item.sponsor}}</div>
                <div class="date">{{item.date}}</div>
                <div class="number">{{item.points}}</div>
                <div class="status">{{item.status}}</div>
                <div class="name">{{item.referal_code}}</div>
                <div class="column">
                    {% if item.name %}
                    <a href="{{root}}admin/member-genealogy/member_id/{{item.id}}" class="button">View Genealogy</a>
                    {% endif %}
                </div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/members/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/members/page/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/members/page/{{page.id}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/members/page/{{page.id}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/members/page/{{next}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/members/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock content %}