{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'admin_nav.tpl.php' %}
<div id="cont-wrap">
	<h1 class="page-title">Purcahse Product</h1>		
			<table class='members-table' align='center'>
				<th width=350>
                    <form action='' method='POST' name='searchpurchase'>
                    SEARCH PRODUCTS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type='text' name='search' placeholder='Search here..' class= 'text mem-txtfld'>&nbsp;
                    <input type='image' src='{{root}}icons/search.png'>
                    </form>
                </th>
				<th width=600>
                    <form action="" method="POST" name="purchaseform">
                    <input type="hidden" value="purchase" name="command">
                    ENTER BUYER'S USERNAME:&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" name="username" placeholder='Enter here' class="mem-txtfld">
                    </form>
                </th>
			</table><br>
			<table class="roboto" align = 'center'>				 
				<tr>
                    <th style='height:50px;' width=100><center>PRODUCT ID</center></th>
				    <th style='height:50px;' width=100><center>NAME</center></th>
				    <th style='height:50px;' width=150><center>DESCRIPTION</center></th>
				    <th style='height:50px;' width=100><center>PRICE</center></th>
				    <th style='height:50px;' width=100><center>STATUS</center></th>
				    <th style='height:50px;' width=100><center>IMAGE</center></th>
				    <th style='height:50px;' width=100><center>DATE ADDED</center></th>
				    <th style='height:50px;' width=100><center>QUANTITY</center></th>
                </tr>
                {% for product in products %}
                <tr>
                    <td style='overflow:hidden; width:100px;' align='center'>{{product.product_id}}</td>
                    <td style='overflow:hidden; width:100px;' align='center'>{{product.name}}</td>
                    <td style='overflow:hidden; width:150px;' align='center'>{{product.description}}</td>
                    <td style='overflow:hidden; width:100px;' align='center'>{{product.price}}</td>
                    <td style='overflow:hidden; width:100px;' align='center'>{{product.status}}</td>
                    <td style='overflow:hidden; width:100px;' align='center'><img src ='{{root}}images/{{product.image}}' width='100' height='100'></td>
                    <td style='overflow:hidden; width:100px;' align='center'>{{product.date_added}}</td>												
                    <td width=100 align = 'center'><input type="text" name="quantity" value="1" maxlength="4" size="4" align="right"/></td>
                </tr>
                {% endfor %}
                <tr>
                    <td colspan=2><p>&nbsp;</p></td>
                    <td align='right' colspan=2><b>TOTAL COST:</b></td>
                    <td align='right' colspan=2><input type='text' name='totalcost' class='text' id='sum' size='16' readonly></td>
                    <td align='center'><input type='reset' name='reset' value='RESET'></td>
                    <td align='center'><input type='submit' name='submit' value='SUBMIT'></td>
                <tr>
            </table>
	</div>
{% endblock content %}