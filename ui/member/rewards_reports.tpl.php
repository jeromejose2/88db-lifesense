{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Reports</h1>
    <br>
    <br>
    <br>
    <div class="order_detail_container" align="center">
        <div class="form_item">
            <div class="form_label"><label>Total Orders (YTD):</label></div>
            <div><input type="text" class="text" value="{{total_sales_per_year}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Weekly Sales for the Month of {{current_month}}</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="topcolumn"><b>Week</b></div>
                        <div class="topcolumn"><b>Total Price</b></div>
                        <div class="topcolumn"><b>Total Items</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in current_month_sales %}
                    <div class="row">
                        <div class="column">{{m.week}}</div>
                        <div class="column">&#x20b1; {{m.total_price}}</div>
                        <div class="column">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales}}</h2></div>                    
                </div>
            </div>
        </div>

        <!--added by Jerome.j-->
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Top 10 Earning Members (Downlines)</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="number"><b>ID</b></div>
                        <div class="name"><b>Name</b></div>
                        <div class="number"><b>Earnings (SP)</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in top_10_earning_members %}
                    <div class="row">
                        <div class="number">{{m.id}}</div>
                        <div class="name">{{m.name}}</div>
                        <div class="number">{{m.points}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>   
        <div class="form_item_div">
            <div>&nbsp;</div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Total Sales (This Month):</label></div>
            <div><input type="text" class="text" value="&#x20b1; {{total_sales_current}}" readonly></div>
        </div>                
        <div class="form_item">
            <div class="form_label"><label>Total Sales (YTD):</label></div>
            <div><input type="text" class="text" value="&#x20b1; {{total_sales_ytd}}" readonly></div>
        </div>  
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Sales per Month</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="short_name"><b>Month</b></div>
                        <div class="number"><b>Total Price</b></div>
                        <div class="number"><b>Total Items Sold</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in per_month_sales %}
                    <div class="row">
                        <div class="short_name">{{m.month_name}}</div>
                        <div class="number">&#x20b1; {{m.total_price}}</div>
                        <div class="number">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales_per_month}}</h2></div>       
                    <br>
                    <br>
                </div>
            </div>
        </div> 










    </div>
</div>
{% endblock content %}