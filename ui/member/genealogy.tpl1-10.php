{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Member Genealogy</h1>
    <div id="members">
        <div class="subcontent">
            <span class="subcontent_heading">Genealogy</span>

            <br>
            <br>
            <div class="toprow">
                <div class="number"><b>Level</b></div>
                <div class="name"><b>Member</b></div>
                <div class="date"><b>Date Joined</b></div>
                <div class="number"><b>Points Earned</b></div>
            </div>
            {% for item in members %}
            <div class="row" id="D{{item.userID}}">
                <div class="number">{{item.level}}</div>
                <div class="name">

                    <img src="{{root}}{{item.image}}" width="40" height="40"/>
                    <a href="#" class="gene_link" id="{{item.userID}}">
                        {{item.name}} 
                    </a>
                </div>
                <div class="date">{{item.date}}</div>
                <div class="number">{{item.points}}</div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first or prev == first %}
                        <a href="#" class="page disable">First</a>
                        {% else %}
                        <a href="{{root}}admin/orders/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first or prev == first %}
                    <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                    <div class="item"><a href="{{root}}admin/orders/page/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                        {% if page.id == current_page %}
                        <a href="{{root}}admin/orders/page/{{page.id}}" class="page active">{{page.id}}</a>
                        {% else %}
                        <a href="{{root}}admin/orders/page/{{page.id}}" class="page gradient">{{page.id}}</a>
                        {% endif %}
                        {% else %}
                        <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last or next == last %}
                    <div class="item"><a href="#" class="page disable">Next</a></div>
                    <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                    <div class="item"><a href="{{root}}admin/orders/page/{{next}}" class="page gradient">Next</a></div>
                    <div class="item"><a href="{{root}}admin/orders/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>


<script language="JavaScript" type="text/javascript">
$(".gene_link").on("click", function() {
    var clickedId= $(".gene_link").attr("id");
    $.post('{{root}}members/GenealogyDownline',{'userID': clickedId.toString()}, function(data){
        var myArray = jQuery.parseJSON(data);
        for(var i=0;i<myArray.length;i++){
            var aID=myArray[i].id;
            var aImage=myArray[i].image;
            var aDate = myArray[i].date;
            var aPoints = myArray[i].points;
            var aName = myArray[i].name;
            
            var displayAppend="<div class='row' id='D"+aID+"'><div class='number'>NUMBER</div><div class='name'><img src='{{root}}"+aImage+"' width='40' height='40'/><a href='#' class='gene_link' id='"+aID+"'>"+aName+"</a></div><div class='date'>"+aDate+"</div><div class='number'>"+aPoints+"</div></div>";
            
            $('#'+clickedId).after(displayAppend);
        }


    });
});
</script>
{% endblock content %}