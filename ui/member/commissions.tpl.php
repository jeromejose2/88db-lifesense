{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Commissions</h1>
    <br>
    <br>
    <br>
    <div class="order_detail_container" align="center">
        <div class="form_item">
        <table>
        <tr>
            <td> 
                <div class="form_label">
                    <!--<label>Your Current Sphere Points ({{start_date}} - {{end_date}}):</label>-->
                </div>
            </td>
            <td>
                <div>
                    <!--<input type="text" class="text" value="{{sphere_points}}" readonly>-->
                </div>
                
            </td>
            <td>
              
				        &nbsp;&nbsp;<a href="{{root}}members/transactions">View Transactions</a></div> 
            </td>
        </tr>
        <tr>
            <td>
                <div class="form_label">
                    <label>Your Current Commission ({{start_date}} - {{end_date}}):</label>
                </div>
            </td>
            <td>
                <div>
                    <input type="text" class="text" value="&#x20b1; {{commission}}" readonly>
                </div>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        </table>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Past Commissions</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="number">Transaction ID</div>
                        <div class="date">From</div>
                        <div class="date">To</div>
                        <div class="number">Commission</div>
                    </div>
                    {% for c in commissions %}
                    <div class="row">
                        <div class="number">{{c.id}}</div>
                        <div class="date">{{c.start_date}}</div>
                        <div class="date">{{c.end_date}}</div>
                        <div class="number">&#x20b1; {{c.income}}</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_commission}}</h2></div>                    
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock content %}