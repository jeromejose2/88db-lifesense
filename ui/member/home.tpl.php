{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}

<div id="cont-wrap">
    <h1 class="page-title">MEMBER DASHBOARD</h1>        
    <ul id="user-main-pane">
        <li>
            <h2> <img src="{{root}}{{image}}" width="60" height="60"> Welcome to your Dashboard {{member_name}} !</h2>
            <p>Your last login was <em>{{last_login}}</em></p>
            <p>Your currently have <em>{{points}} BV</em></p>
        </li>            
        <li>
            <ul id="ump-opts">
                <a href="{{root}}members/changepassword"><li><strong>+</strong>Change Password</li></a>
                <a href="{{root}}members/editaccount"><li><strong>+</strong>Edit Account</li></a>
                <a href="{{root}}members/rewards-reports"><li><strong>+</strong>View Reports</li></a>
                <a href="{{root}}sites/sites"><li><strong>+</strong>View Your Site</li></a>
                
            </ul>
        </li>
    </ul>        
    <ul id="user-accordion-pane">
        <li>
            <a href="" id="personal-det" class="accord">
                <h3><strong class="accord"></strong>Company Announcements</h3>
            </a>
            <div class="accopen personal-det" id="home-acc">
             
           
                 <table class="tAnnouncement">
                    <thead><tr class='tHead'><th class="tDate">Date</th><th class="tTitle">Title</th><th class="tDescription">Description</th></thead></tr>
                    {% for ann in announcement %}
                    <tr class="tBody">
                        <td>{{ann.datex}}</td>
                        <td>{{ann.titlex}}</td>
                        <td>{{ann.descriptionx}}</td>
                    </tr>
                     {% endfor %}
                </table>
                <div class="clearfix"></div>
         
            <br/>
           
        </div>
    </li>
    <li>
        <a href="" id="statistics"  class="accord"><h3><strong class="accord"></strong>Genealogy</h3></a>
        <div class="accopen statistics">
            <ul class="info">
                <li class="odd"><strong>Downlines:</strong> <p><a href="{{root}}members/genealogy">(view)</a></p></li>
                <div class="clearfix"></div>
            </ul>
        </div>
    </li>
</ul>
</div>
{% endblock content %}