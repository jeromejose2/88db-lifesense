{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}	
<?php
//    $myurl = $_SERVER['QUERY_STRING'];
// echo $myurl;
// $myArray = explode('=', $myurl);
// $value = $myArray[2];
// echo $value; 
?>
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <div class="dComposeMsg">
        <h1 class="page-title">Compose Message</h1>
        <br/>
        <br/>
        <br/>
        <div id="common_table_container">
            <div class="subcontent">
                <span class="subcontent_heading">Compose Message</span>
                <br/>
                <br/>
                <form action="{{root}}members/compose/id/{{tox}}/action/post" method="post">

                    <table class="tConversation">

                        {% for con in rsConversation %}
                        <tr>
                            <td class="tdSpeaker">{{con.speaker}} : </td>
                            <td class="tdDate">{{con.date}}</td>
                            <td class="tdMessage">{{con.message}}</td>

                        </tr>
                        {%endfor%}
                    </table>
                    <br/>
                    <br/>

                    <div class="form_label"><label>To: </label></div>
                    <div><img src="{{root}}{{image_directory}}" length="40px" width="40px">{{fullname}}</div> 



                    <div class="form_item">
                        <div class="form_label"><label>Message </label></div>
                        <!--<div><input type="input" class="text" name="message" ID="message" maxlength="32" required></div>-->
                        <div>      <textarea class="text" style="width: 500px;margin-left: 50px;" name="message" id="message" required></textarea></div>
                    </div>

                    <div class="form_item">
                        <div><button type="submit" class="form_button">Send</button></div>
                    </div>
                </form>
            </div>    
        </div><!--common table container-->  
    </div>  <!--class dComposeMsg-->  
</div><!-- content-wrap -->  
{% endblock content %}