{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
    <div id="cont-wrap">
        <br>
        <br>
        <br>
        <br>
        <div class="common_product_container">
            <div class="shopcart_left_container">
                <div>
                    <form id="checkout_form" action="{{root}}members/shop/action/checkout" method="post">
                        <fieldset>
                            <div class="form_item_div">
                                <div class="normal_label"><label>Billing Information</label></div>
                            </div> 
                            <div class="form_item">
                                <div class="form_label"><label>First Name: </label></div>
                                <div><input type="text" class="text" name="firstname" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>Last Name: </label></div>
                                <div><input type="text" class="text" name="lastname" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>Contact No.: </label></div>
                                <div><input type="text" class="text" name="phone_no" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>Mobile No.: </label></div>
                                <div><input type="text" class="text" name="mobile_no"></div>
                            </div>                 
                            <div class="form_item">
                                <div class="form_label"><label>E-Mail: </label></div>
                                <div><input type="text" class="text" name="email" required></div>
                            </div>                            
                            <div class="form_item_div">
                                <div class="normal_label"><label>Billing Address</label></div>
                            </div>                            
                            <div class="form_item">
                                <div class="form_label"><label>Street: </label></div>
                                <div><input type="text" class="text" name="billing_street" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>City: </label></div>
                                <div><input type="text" class="text" name="billing_city" required></div>
                            </div>                            
                            <div class="form_item">
                                <div class="form_label"><label>State: </label></div>
                                <div><input type="text" class="text" name="billing_state" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>Country: </label></div>
                                <div><select name="billing_country">
                                        <option value="PH">Philippines</option>
                                     </select>
                                </div>
                            </div>
                            <div class="form_item_div">
                                <div class="normal_label"><label>Shipping Information</label></div>
                            </div>                            
                            <div class="form_item">
                                <div class="form_label"><label>Street: </label></div>
                                <div><input type="text" class="text" name="shipping_street" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>City: </label></div>
                                <div><input type="text" class="text" name="shipping_city" required></div>
                            </div>                            
                            <div class="form_item">
                                <div class="form_label"><label>State: </label></div>
                                <div><input type="text" class="text" name="shipping_state" required></div>
                            </div>
                            <div class="form_item">
                                <div class="form_label"><label>Country: </label></div>
                                <div><select name="shipping_country">
                                        <option value="PH">Philippines</option>
                                     </select>
                                </div>
                            </div>
                            <div class="form_item">
                                <div><button type="submit" class="form_button">Submit</button></div>
                            </div>
                        </fieldset>
                    </form>    
                </div>
            </div>
            <div class="shopcart_right_container">
                <button type="button" id="continue_shopping" class="form_button" value="{{root}}members/shop">Continue Shopping</button>
            </div>
        </div>
    </div>
{% endblock content %}