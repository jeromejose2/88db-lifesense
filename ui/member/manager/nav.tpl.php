        <header>
            <div class="navigation main-nav">
                <ul id="main-nav">
<!--                    <a href="http://sphere.ph/tomato"><li class="top tptomato"></li></a>
                    <a href="http://sphere.ph/time"><li class="top tptime"></li></a>
                    <a href="http://sphere.ph/swap"><li class="top tpswap"></li></a>
                    <a href="http://sphere.ph"><li>Join Sphere Now!</li></a>-->
                    {% if show_cart_items %}
                    <a href="#"><li class="tpitems"><strong class="cart-ico"></strong>Items in Cart (0)</li></a>
                    {% endif %}
                </ul>
            </div>
            <div class="navigation sub-nav">
            	<ul id="sub-nav">
                    <div id="banner">
                        <h1 class="web-ban"></h1>
                    </div> 
                    <li><a href="{{root}}manager/home">Home</a></li>
                    <li><a href="{{root}}manager/members">Members</a></li>
                    <li><a href="{{root}}manager/registration">Registration</a></li>
                    <li><a href="{{root}}manager/commissions">Commissions</a></li>
                    <li><a href="{{root}}manager/rewards-reports">Reports</a></li>
                    <li id="settings"><a href="{{root}}manager/account-settings">Settings</a></li>
                    <li id="logout"><a href="{{root}}logout">Logout</a></li>                    
                </ul>
            </div>
        </header>
