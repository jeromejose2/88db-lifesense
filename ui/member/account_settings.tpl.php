{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Settings</h1>
    <br>
    <br>
    <br>
    <br>    
    <div class="common_table_container">
        <div>
            <button id="changepass" value="{{root}}members/changepassword" class="form_button">Change Password</button>    
        </div>
        <div>
            <button id="editaccount" value="{{root}}members/editaccount" class="form_button">Edit Account</button>
        </div>        
    </div>
</div>
{% endblock content %}