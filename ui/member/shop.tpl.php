{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}

<div id="cont-wrap">
    <br>
    <br>
    <br>
    <br>
    <div class="common_product_container">

        {% block foo %}
        {% if admin_user == true %}
        <div style="" align="right">
            <!--<a href="{{root}}members/shop/action/AddItemShop"><strong>+</strong>Add Item in Shop</a>-->
            <button id="view_cart" type="button" value="{{root}}members/shop/action/AddItemShop" class="form_button">Add Items in Shop</button>    
        </div>
        {% endif %}
        {% endblock %}

        <div class="shopcart_category_container">
            <ul class="category-list">
                <a class="category-item" href="{{root}}members/shop"><li>All</li></a>
                {% for cat in category_list %}
                <a class="category-item" href="{{root}}members/shop/cat/{{cat.id}}"><li>{{cat.title}}</li></a>
                {% endfor %}
            </ul>
        </div>
        <div class="shopcart_left_container">
            <div>
                {% for item in products %}
                <div class="product_item" align="center">
                    <a href="{{root}}members/shop/action/view/id/{{item.id}}"><img src="{{root}}{{item.photo}}" style="width: 120px"/></a>
                    <br>
                    <br>
                    <a href="{{root}}members/shop/action/view/id/{{item.id}}" class="order_item_link">{{item.name}}</a><br>
                    <b>Price:</b> &#x20b1; {{item.price}}<br>
                </div>
                {% endfor %}
            </div>
        </div>
        <div class="shopcart_right_container">
            <button type="button" id="view_cart" class="form_button" value="{{root}}members/shop/action/viewcart">View Your Cart</button>
        </div>
    </div>
</div>
{% endblock content %}