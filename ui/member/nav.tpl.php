<header>
    <div class="navigation main-nav">
        <ul id="main-nav">
            <!--                    <a href="http://sphere.ph/tomato"><li class="top tptomato"></li></a>
                                <a href="http://sphere.ph/time"><li class="top tptime"></li></a>
                                <a href="http://sphere.ph/swap"><li class="top tpswap"></li></a>-->
            <!--<a href="http://sphere.ph"><li>Join Sphere Now!</li></a>-->
            <a href="{{root}}logout"><li>Logout</li></a>
            {% if show_cart_items %}
            <a href="#"><li class="tpitems"><strong class="cart-ico"></strong>Items in Cart ({{shop_cart_items}})</li></a>
            {% endif %}
        </ul>
    </div>
    <div class="navigation sub-nav">
        <div id="banner" style="margin-left: 170px;cursor: pointer;" onclick="location.href='{{root}}members/home';" >
            <h1 class="web-ban"></h1>
        </div> 
        <ul id="sub-nav">
            <li><a href="{{root}}members/home">Home</a></li>
            <li><a href="{{root}}members/messages">Inbox</a></li>
            <li><a href="{{root}}members/genealogy">Genealogy</a></li>
            <li><a href="{{root}}members/commissions">Commissions</a></li>
            <li><a href="{{root}}members/transactions">Transactions</a></li>
            <li><a href="{{root}}members/rewards-reports">Reports</a></li>
            <!--<li><a href="http://localhost/sphere">Shop</a></li>      -->              
            <li><a href="{{root}}members/shop/view">Shop</a></li>
            <li id="settings">
                <a href="#">Settings</a>
                <ul class="sublets settings">
                    <li><a href="{{root}}members/account-settings">Account Settings</a></li>
                </ul>                        
            </li>

        </ul>
    </div>
</header>
