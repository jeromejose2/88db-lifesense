{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}
<head>
    <script type="text/javascript">
        $ (document).ready (function() {

    </script>
</head>
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <br>

    <form action="{{root}}members/shop/action/AddItemShop" method="post" enctype="multipart/form-data">
        <div class="form_item" style="">Item Category : &nbsp;&nbsp;&nbsp;&nbsp;
            <select name='shopcategoryid' class="text">
                {% for usr in rsUser %}
                <option  value="{{usr.id}}">{{usr.title}}</option>

                {% endfor%}
            </select>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Item Picture </label></div>
            <div><input type="file" name="file" id="file"><br></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Item Name: </label></div>
            <div><input type="" class="text" name="itemname" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Item Description: </label></div>
            <div><input type="" class="text" name="itemdesc" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Item Prize: </label></div>
            <div><input type="" class="text" name="itemprize" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Item Stock: </label></div>
            <div><input type="" class="text" style="width: 50px;"name="itemstock" maxlength="5" onkeypress="isnumberkey(event);"required></div>
        </div>
        <div class="form_item">
            <div><button type="submit" class="form_button">Add Item</button></div>
        </div>
    </form>

    <div id="root" style="display:none">{{root}}</div>

    <script type="text/javascript">
                function isAlphaNumericKey(evt)
                {

                var charCode = (event.which) ? event.which : event.keyCode;
                        if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
                {
                return false;
                }
                return true;
                }
        /*Added by JFJ*/
        function isnumberkey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode == 49 || charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
        }
        return true;
        }
    </script>
    {% endblock content %}