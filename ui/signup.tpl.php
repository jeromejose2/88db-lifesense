{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "nav.tpl.php" %}
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <br>
    <div class="common_form_container">
        <center><h1 class="page-title" style="color: black">Registration Form</h1></center>
        <br>
        <form id="signup_form" action="{{root}}signup" method="post">
            <fieldset>
                <div class="field_item_div">
                    <div class="normal_label"><label>Personal Information</label></div>
                </div>                        
                <div class="field_item">
                    <label>Name<span>*</span></label><br>
                    <input type="text" name="firstname" class="text" placeholder="First Name" required/>
                    <input type="text" name="lastname" class="text" placeholder="Last Name" required/>
                    <input type="text" name="mi" size="3" class="text" placeholder="M.I." required/>
                </div>
                <div class="field_item">
                    <label>Home Address<span>*</span></label><br>
                    <input type="text" name="street" class="text" placeholder="Street" size="50" required/>
                    <input type="text" name="city" class="text" placeholder="City" required/>
                    <input type="text" name="state" class="text" placeholder="State" required/>
                    <select name="country">
                        <option value="PH">Philippines</option>
                    </select>
                </div>
                <div class="field_item">
                    <label>Phone No<span>*</span></label><br>
                    <input type="text" class="text" name="phone" required/>
                </div>
                <div class="field_item">
                    <label>Mobile No<span>*</span></label><br>
                    <input type="text" class="text" name="mobile" required/>
                </div>
                <div class="field_item">
                    <label>E-Mail<span>*</span></label><br>
                    <input type="text" class="text" name="email" required/>
                </div>
                <div class="field_item">
                    <label>Gender</label><br>
                    <select name="gender">
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="field_item">
                    <label>Date of Birth</label><br>
                    <select name="birth_day">
                        {% for day in days %}
                        <option value="{{day}}">{{day}}</option>
                        {% endfor %}
                    </select>
                    <select name="birth_month">
                        {% for month in months %}
                        <option value="{{month.id}}">{{month.name}}</option>
                        {% endfor %}
                    </select>
                    <select name="birth_year">
                        {% for year in years %}
                        <option value="{{year}}">{{year}}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="field_item">
                    <label>Marital Status</label><br>
                    <select name="marital_status">
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                        <option value="3">Divorced</option>
                        <option value="4">Widowed</option>                            
                    </select>
                </div>



                <!--    comment unnecessary fields-jerome.j            <div class="field_item">
                                    <label>TIN</label><br>
                                    <input type="text" class="text" name="tin"/>
                                </div>
                                <div class="field_item">
                                    <label>Passport</label><br>
                                    <input type="text" class="text" name="passport"/>
                                </div>                        
                                <div class="field_item">
                                    <label>Driver's License</label><br>
                                    <input type="text" class="text" name="drivers_license"/>
                                </div>                        
                
                                <div class="field_item">
                                    <input type="text" class="text" name="spouse_name" placeholder="Name Of Spouse"/><input type="text" class="text" name="spouse_contact_no" placeholder="Contact No."/><br>
                                </div>                                                
                                <div class="field_item_div">
                                    <div class="normal_label"><label>Beneficiaries</label></div>
                                </div>
                                <div class="field_item">
                                    <input type="text" class="text" name="beneficiary1_name" placeholder="Name"/><input type="text" class="text" name="beneficiary1_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary1_contact_no" placeholder="Contact No."/><br>
                                    <input type="text" class="text" name="beneficiary2_name" placeholder="Name"/><input type="text" class="text" name="beneficiary2_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary2_contact_no" placeholder="Contact No."/><br>
                                    <input type="text" class="text" name="beneficiary3_name" placeholder="Name"/><input type="text" class="text" name="beneficiary3_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary3_contact_no" placeholder="Contact No."/><br>
                                </div>                        
                                <div class="field_item">
                                    <label>Office Address</label><br>
                                    <input type="text" class="text" name="office_street" placeholder="Street" size="50"/>
                                    <input type="text" class="text" name="office_city" placeholder="City"/><br>
                                    <input type="text" class="text" name="office_state" placeholder="State"/><br>
                                    <select name="office_country">
                                        <option value="PH">Philippines</option>
                                    </select>
                                </div>
                                <div class="field_item">
                                    <label>Shipping Address</label><br>
                                    <input type="text" class="text" name="shipping_street" placeholder="Street" size="50"/>
                                    <input type="text" class="text" name="shipping_city" placeholder="City"/><br>
                                    <input type="text" class="text" name="shipping_state" placeholder="State"/><br>
                                    <select name="shipping_country">
                                        <option value="PH">Philippines</option>
                                    </select>
                                    <input type="text" class="text" name="zipcode" placeholder="Zip Code">
                                </div>
                                <div class="field_item_div">
                                    <div class="normal_label"><label>Other Contact</label></div>
                                </div>
                                <div class="field_item">
                                    <input type="text" class="text" name="facebook_id" placeholder="Facebook ID "/>
                                    <input type="text" class="text" name="skype_id" placeholder="Skype ID"/>
                                    <input type="text" class="text" name="twitter_id" placeholder="Twitter ID"/>
                                </div>
                                <div class="field_item">
                                    <label>Member Of Other MLM Company?</label>
                                    <input type="checkbox" name="member_of_other_mlm"/><br>
                                </div>
                                <div class="field_item_div">
                                    <div class="normal_label"><label>Where to Deposit Your Commissions</label></div>
                                </div>
                                <div class="field_item">
                                    <label>Bank Details</label><br>
                                    <input type="text" class="text" name="bank_name" placeholder="Bank Name"/>
                                    <input type="text" class="text" name="bank_account_name" placeholder="Bank Account Name"/>
                                    <input type="text" class="text" name="bank_account_no" placeholder="Bank Account Number"/>
                                    <input type="text" class="text" name="bank_branch" placeholder="Branch Name"/>
                                </div>-->



                <div class="field_item_div">
                    <div class="normal_label"><label>Sponsor Information</label></div>
                </div>                        
                <div>
                    <table class="hovertable" style='table-layout:fixed;' align = 'center'>				 
                        <tr>
                            <th style='height:50px;' width=100><center>IMAGE</center></th>
                        <th style='height:50px;' width=100><center>NAME</center></th>
                        <th style='height:50px;' width=150><center>DESCRIPTION</center></th>
                        <th style='height:50px;' width=100><center>PRICE</center></th>
                        <th style='height:50px;' width=250><center>ENTER SPONSOR NAME & CODE NUMBER<br><font face = 'Verdana' size = 1 color = 'red'><center>(required)</font></center></th></tr><tr>
                                <!--                                </table>
                                                            <table class = 'hovertable'  style='table-layout:fixed;' align = 'center'>-->
                                {% for item in kitproducts %}
                                <td style='overflow:hidden; width:100px;' align='center'><img src = '{{root}}images/products/{{item.image}}' width = 100 height = 100></td>
                                <td style='overflow:hidden; width:100px;' align='center'>{{item.name}}</td>
                                <td style='overflow:hidden; width:150px;' align='center'>{{item.description}}</td>
                                <td style='overflow:hidden; width:100px;' align='center'>{{item.price}}</td>
                                <td style='overflow:hidden; width:250px;' align='center'>
                                    <table class = 'hovertable'>
                                        <tr onmouseover="this.style.backgroundColor = '#ffff66';" onmouseout="this.style.backgroundColor = '#d4e3e5';">
                                            <td><font face = 'Verdana' size = '2'>Sponsor:</td>
                                            <td><input type="text" id="tbSponsor"  onpaste="return false;"  onchange="displaySponsor()" maxlength="10" class="text" name="sponsor" required/>
                                            </td>
                                            <td> <div id='displayName' style="width: 150px;"></div></td>
                                        </tr>
                                        <tr onmouseover="this.style.backgroundColor = '#ffff66';" onmouseout="this.style.backgroundColor = '#d4e3e5';">
                                            <td><font face = 'Verdana' size = '2'>CODE #:</td>
                                            <td><input type="text" class="text" maxlength="10"  onpaste="return false;" name="activation_code" id="activation_code"   onchange="displayCode()" required/></td>
                                            <td> <div id='displayCode' style="width: 150px;"></div></td>
                                    </table>
                                </td>
                                {% endfor %}
                                </table>
                                </div>

                                <!--                            <div class="field_item">
                                                                <label>Sponsor<span>*</span></label><br>
                                                                <input type="text" id="tbSponsor" onKeyUp="displaySponsor()" class="text" name="sponsor" required/>
                                                                <div id='displayName'></div>
                                                            </div>
                                                            <div class="field_item">
                                                                <label>Activation Code<span>*</span></label><br>
                                                                <input type="text" class="text" name="activation_code" required/>
                                                            </div>-->

                            <div class="common_product_container">
                                <div class="shopcart_left_container" style="margin-left: 200px;">
                                    {% for item in products %}
                                    <input type = 'hidden' name = 'counter' value ='{{item.counter}}'>
                                    <input type = 'hidden' name = 'name{{loop.index}}' value ='{{item.name}}'>
                                    <input type = 'hidden' name = 'price{{loop.index}}' value ='{{item.price}}'>
                                    <div class="product_item" align="center">
                                        <img src="{{root}}{{item.photo}}" style="width: 120px" title="{{item.description}}"/>
                                        <br>
                                        <br>
                                        <a href="{{root}}">{{item.name}}</a><br>
                                        <b>Price:</b> P {{item.price}}<br>
                                        <b>Quantity: </b>
                                        <select id="qty{{loop.index}}" name="qty{{loop.index}}" class="quantity"  OnBlur="getComboVal({{loop.index}},this)">
                                            {% for n in item.qty %}
                                            {% if n == 0 %}
                                            <option value="0" selected>{{n}}</option>
                                            {% else %}
                                            <option name ="{{item.name}}" value="{{(loop.index -1) * item.price}}">{{n}}</option>
                                            {% endif %}
                                            {% endfor %}
                                        </select>
                                        <br>
                                    <!--<input type="button" class="button" name="add_to_cart" value="Add To Cart"/>-->
                                    </div>
                                    {% endfor %}
                                </div>
                                <!--                    <div class="shopcart_right_container">
                                                        <input type="button" name="continue_shopping" value="Continue Shopping"/>
                                                        <input type="button" name="view_cart" value="View Your Cart"/>
                                                    </div>-->
                            </div>


                            <div class="field_item">
                                <input type = 'text' name = 'total' class = 'text' id='sum' size = '10' readonly>
                            </div>

                            <div class="field_item">
                                <br>
                                <br>
                                <input type="checkbox" name="agree"/>
                                <label>I agree to the <a href="{{root}}help/terms-and-condition" target="_blank">Terms Of Service.</a></label>
                            </div>
                            <div class="field_item"><button type="submit" name="submit" class="form_button">Register</button></div>
                            </fieldset>
                            </form>
                            </div>
                            </div>

                            <script type="text/javascript">
                                function getComboVal(id,sel)
                                {
                                var getValue=$(sel).val();
                                var id = "qty"+id;
                                alert(id);
                                alert(getValue);
                                if(getValue != 0)
                                {
//                                     $.post('{{root}}members/ActivateCode', {'code': inputID.toString()}, function(data){
//                                        var myArray = jQuery.parseJSON(data);
//                                                if (myArray == true)
//                                                    {
//                                                    dDisplayName.innerHTML = "Code Accepted";
//                                                    } else
//                                                    {
//                                                    dDisplayName.innerHTML = "Code is not Valid";
//                                                    }
//                                        });
                                }
                                           
                                }
                    $(document).on('change', '.quantity', function(){
                        var total = 0;
                         
                            $('.quantity').each(function()
                            {
                                total += parseInt($(this).val());
                            });
                            $('#sum').val(total);
                    });
                                        function displaySponsor()
                                        {
                                        var dDisplayName = document.getElementById("displayName");
                                                var idList = [];
                                                var userList = [];
                                                var inputID = document.getElementById('tbSponsor').value;
                                        {% for id in idList %}

                                        idList.push("{{id.id}}");
                                                userList.push("{{id.fullname}}");
                                        {% endfor %}
                                        var indexInput = idList.indexOf(inputID);
                                                if (indexInput != - 1)
                                        {
                                        dDisplayName.innerHTML = userList[indexInput];
                                        } else
                                        {
                                        dDisplayName.innerHTML = "INVALID SPONSOR";
                                        }
                                        }

                                function displayCode()
                                {
                                var dDisplayName = document.getElementById("displayCode");
                                        var inputID = document.getElementById('activation_code').value;
                                        dDisplayName.innerHTML = "";
                        $.post('{{root}}members/ActivateCode', {'code': inputID.toString()}, function(data){
                                        var myArray = jQuery.parseJSON(data);
                                                if (myArray == true)
                                                    {
                                                    dDisplayName.innerHTML = "Code Accepted";
                                                    } else
                                                    {
                                                    dDisplayName.innerHTML = "Code is not Valid";
                                                    }
                                        });
                                }
                            </script>
                            {% endblock content %}