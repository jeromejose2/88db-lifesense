{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}
<?php
//    require_once'../../ui/header.tpl.php';
//    require_once'../../ui/member/nav.tpl.php';
?>
<header>
    <script>
        $ (document).ready (function() {
        });
    </script>
</header>
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <h1 class="page-title">Build Your Site</h1>
    <div class="common_table_container" align="center">
        <br/>
        <br/>       
        <form action="{{root}}sites/createsite/action/create" method="post">
            <div class="form_item">
                <div class="form_label"><label>Store User Name: </label></div>
                <div><label>{{member_name}}</label></div>
                <input type="hidden" name="sitename" value="{{member_name}}"/>
            </div>
            <br/>
            <br/>
            <div class="form_item">
                <div class="form_label"><label>Store URL: </label></div>
                <!--<div><input class="text" name="newpass" maxlength="32" required></div>-->
                <div><label>{{member_websitename}}</label></div>
                <input type="hidden" name="siteurl" value="{{member_websitename}}"/>
            </div>
            <br/>
            <br/>
            <div class="form_item">
                <div class="form_label"><label>Tell Us More About Your Site : </label></div>
                <br/>
                <textarea name="storedescription" id="storedescription" style="border-radius: 3%;width: 500px;height: 200px;background: #f9f9f9;" required></textarea>

            </div>
            <br/>
            <br/>
            <div class="form_item">
                <div class="form_label"><label>Terms & Conditions : </label></div>
                <!--<div><input class="text" name="newpass" maxlength="32" required></div>-->
                <div>&nbsp;
<!--                    <input  type="radio" id="radiotc-merchant" name="radiotc"  onClick="$ ('#tc-merchant').show ();
                            $ ('#tc-uniqly').hide ();" checked="true"/>Create your own
                    <input  type="radio" id="radiotc-uniqly" name="radiotc" onClick="$ ('#tc-uniqly').show ();
                            $ ('#tc-merchant').hide ();"/>Use Uniqly Terms and Condition-->
                </div>
            </div>
            <div class="" style="border-radius: 5px;height: 250px;overflow: hidden;font-size: 85%;padding: 2% 3% 2% 2%;background: #f9f9f9; margin: 2% 0; border: none;  font-weight: 400;overflow-y: scroll;">
                <div class="tc-cont" style="">
                    <p>Your use and visit of uniqly.net is subject to your complete submission and compliance to the terms and conditions presented in this uniqly.net User Agreement (uniqly.net UA). This uniqly.net UA may be modified from time to time to serve the best interest of uniqly.net.</p>

                    <p>1. uniqly.net<br/>Uniqly.net is a user-friendly Filipino-centric e-commerce platform. This website with URL: www.uniqly.net  allows you to create online stores where you can conveniently upload & sell products.</p>

                    <p>2. Your Account<br/>To create and maintain your account in uniqly.net, it is understood that you:
                    <p>&nbsp; 1. Have read, accepted and agreed to all the terms presented in this <strong>uniqly.net</strong> UA which applies to your visit and use of any and all services of <strong>uniqly.net</strong></p>
                    <p>&nbsp; 2. Have submitted only true and accurate information about you to <strong>uniqly.net</strong> and will make sure to keep this information up-to-date</p>
                    <p>&nbsp; 3. Are of legal age and therefore can enter into any legal transaction with <strong>uniqly.net</strong> and <strong>its</strong> users</p>
                    <p>&nbsp; 4. Are in charge of keeping the confidentiality of your account details and therefore will not share your password or any account-related information that might result in unauthorized access or use of your account</p>
                    <p>&nbsp; 5. <strong>Being a uniqly.net User</strong></p>
                    </p>

                    <p>As a buyer or seller, you understand and agree that <strong>uniqly.net</strong> can and will temporarily or permanently suspend your account or, without prior notice, remove items you upload if you violate any of the terms in this 88UA. Therefore, you submit yourself to this uniqly.net UA and will not:</p>

                    <p>
                    <p>&nbsp; 1. Do anything that goes against the spirit of <strong>uniqly.net</strong></p>
                    <p>&nbsp; 2. Upload viruses or use technology that can undermine <strong>uniqly.net</strong></p>
                    <p>&nbsp; 3. Intentionally or unintentionally, provide false, inaccurate, misleading information about yourself during registration, visit and usage of <strong>uniqly.net</strong></p>
                    <p>&nbsp; 4. Provide or claim information about other people as your own for any reason</p>
                    <p>&nbsp; 5. Give any information that may violate someone's privacy</p>
                    <p>&nbsp; 6. Allow someone else to use or transfer your account without written consent of <strong>uniqly.net</strong>.</p>
                    <p>&nbsp; 7. Share your login credentials or any information that can compromise the integrity and security of your account</p>
                    <p>&nbsp; 8. Ask other members for their login credentials or other information that can compromise the integrity and security of their account</p>
                    <p>&nbsp; 9. Collect or spread information about users and visitors of <strong>uniqly.net</strong> for ill purposes such as pyramid schemes, spam, chain letters and other similar/related schemes</p>
                    </p>

                    <p><strong>If you are a SELLER, you will not:</strong></p>

                    <p>
                    <p>&nbsp; 10. Post or upload products that are unsuitable or opposed to the general principles of <strong>uniqly.net</strong></p>
                    <p>&nbsp; 11. Post or upload products that are sexual, violent, offensive to one's ethnicity, religion or beliefs or anything offensive to the general public</p>
                    <p>&nbsp; 12. Unfairly price your products</p>
                    <p>&nbsp; 13. Post or upload products that you cannot deliver</p>
                    <p>&nbsp; 14. Default from your obligations with buyers of your products unless, despite doing everything you can to communicate with the buyer,:
                    <p>&nbsp; &nbsp; 1. The identity of the buyer cannot be determined</p>
                    <p>&nbsp; &nbsp; 2. The buyer's address was vaguely or inaccurately submitted during purchase</p>
                    </p>
                    <p>&nbsp; 15. Hold <strong>uniqly.net</strong> responsible for whatever may come from the buyers' purchase of your product such as false, bad, or inaccurate reviews</p>
                    <p>&nbsp; 16. Intentionally or unintentionally give an false or inaccurate description of your product e.g. size, weight, dimension, purpose</p>
                    <p>&nbsp; 17. Default from your obligations like paying <strong>uniqly.net</strong> additional charges arising from letters E & G</p>
                    <p>&nbsp; 18. Withhold any documents, materials and accessories necessary for the buyer to receive and consume/use the products availed from you</p>
                    <p>&nbsp; 19. Sell any of the following prohibited items:
                    <p>&nbsp; &nbsp; 1. Alcoholic Beverages</p>
                    <p>&nbsp; &nbsp; 2. Animal Skins, Non-Domesticated</p>
                    <p>&nbsp; &nbsp; 3. Articles of <strong>uniqly.net</strong> UA Value</p>
                    <p>&nbsp; &nbsp; 4. Dangerous Goods/Hazardous Materials</p>
                    <p>&nbsp; &nbsp; 5. Firearms</p>
                    <p>&nbsp; &nbsp; 6. Furs</p>
                    <p>&nbsp; &nbsp; 7. Live Animals</p>
                    <p>&nbsp; &nbsp; 8. Perishables</p>
                    <p>&nbsp; &nbsp; 9. Personal Effects</p>
                    <p>&nbsp; &nbsp; 10. Plants</p>
                    <p>&nbsp; &nbsp; 11. Pornographic Materials</p>
                    <p>&nbsp; &nbsp; 12. Seeds</p>
                    <p>&nbsp; &nbsp; 13. Tobacco</p>
                    <p>&nbsp; &nbsp; 14. Unaccompanied Baggage</p>
                    </p>
                    </p>

                    <p>If you are a BUYER, you will not:
                    <p>&nbsp; 20. Give inaccurate or vague information or any information that will cause the seller to be unable to deliver the products you bought</p>
                    <p>&nbsp; 21. Lie about your experience as a buyer</p>
                    <p>&nbsp; 22. Hold <strong>uniqly.net</strong> responsible for products you purchased from members of <strong>uniqly.net</strong> that are of bad quality</p>
                    <p>&nbsp; 23. Use any information you get from Sellers for malicious purposes</p>
                    <p>&nbsp; 24. Buy any of the following prohibited articles:
                    <p>&nbsp; &nbsp; 1.	Alcoholic Beverages</p>
                    <p>&nbsp; &nbsp; 2.	Animal Skins, Non-Domesticated</p>
                    <p>&nbsp; &nbsp; 3.	Articles of <strong>uniqly.net</strong> UA Value</p>
                    <p>&nbsp; &nbsp; 4.	Dangerous Goods/Hazardous Materials</p>
                    <p>&nbsp; &nbsp; 5.	Firearms</p>
                    <p>&nbsp; &nbsp; 6.	Furs</p>
                    <p>&nbsp; &nbsp; 7.	Live Animals</p>
                    <p>&nbsp; &nbsp; 8.	Perishables</p>
                    <p>&nbsp; &nbsp; 9.	Personal Effects</p>
                    <p>&nbsp; &nbsp; 10. Plants</p>
                    <p>&nbsp; &nbsp; 11. Pornographic Materials</p>
                    <p>&nbsp; &nbsp; 12. Seeds</p>
                    <p>&nbsp; &nbsp; 13. Tobacco</p>
                    <p>&nbsp; &nbsp; 14. Unaccompanied Baggage</p>
                    </p>
                    <p>&nbsp; 25. <strong>Fees and Charges</strong><br/>Buyers and sellers are encouraged to join uniqly.net. Registration, which entitles sellers to create and maintain store/s, is absolutely free. Our business model allows uniqly.net to waive fees on transactions.</p>
                    </p>
                    <p>5. <strong>Your Privacy</strong><br/>Your privacy is very important to us at uniqly.net Information you provide is and will remain private and will not be sold or shared with any third party without your permission. You can change the information you provide us anytime. This Privacy Policy explains in detail how we protect your information and the times when we need to share them.</p>

                    <p>6. <strong>Communicating with uniqly.net.</strong><br/>When you communicate with us electronically, you are giving your permission to receive emails from uniqly.net to reply to your message or to receive emails on matters we need to communicate with you on such as a message for you from another member, confirmation of your order, an email alert on someone's order from your store and other automated messages sent by uniqly.net about a service you subscribed to which you can deactivate if you wish.</p>

                    <p>7. <strong>Limitation of Liability</strong><br/>During registration it is understood that both Buyers and Sellers have agreed to:
                    <p>&nbsp; 1. Give only true and accurate information and descriptions of the products uploaded in their stores</p>
                    <p>&nbsp; 2. Sell only products they can deliver</p>
                    <p>&nbsp; 3. Submit all information needed to aid the delivery of the products they bought and communicate with the seller</p>
                    <p>&nbsp; 4. Post only truthful product comments/testimonials on the products they bought</p>
                    </p>

                    <p><strong>Uniqly.net</strong> does not guarantee a satisfactory experience on the quality and delivery of the products you are buying, accuracy and decency of comments and testimonials on the product you are selling and provision of all necessary buyer information for hassle-free delivery.  Therefore, you do not hold <strong>uniqly.net</strong>. responsible for your experience resulting from your transaction with uniqly.net’s buyers and/or sellers.</p>
                </div>

            </div>
            <div class="form_item">
                <div><button type="submit" class="form_button">Create</button></div>
            </div>
        </form>    
    </div>    
</div>
{% endblock content %}


