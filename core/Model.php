<?php
 /*
  * Developed by Cris del Rosario
  */

 require_once 'core/Gui.php';
 require_once 'core/Db.php';
 require_once 'core/Session.php';
 require_once 'core/User.php';
 require_once 'core/Tables.php';
 require_once 'lib/fpdf/fpdf.php';
 // require_once 'lib/PHPExcel/Writer/Excel2007.php';

 abstract class Model {
    protected $TAG = "";
    
    public static $PAGE_NAME_LIMIT = 60;
    private static $extras = array();
     
    public function __construct($class) {	    
        $this->TAG = get_class($class);
        
        GUI::init();
	}
	 
	public function getTag() {
	    return $this->TAG;
	}
	
	public function getExtras() {
        return self::$extras;
	}
	
	public function setExtras($extras) {
        self::$extras = null;
        if ($extras != "") {
            if (substr($extras,0,1) == "/") {
                 $extras = substr($extras,1);
            }
            $e = explode("/",$extras);
            if (is_array($e) && count($e) > 0) {
                self::$extras = array();
                $index = 1;
                $key = "";
                foreach ($e as $n) {
                    if (($index % 2) == 0 && $index > 0) {
                        self::$extras[$key] = $n;
                    } else {
                        $key = $n;
                        self::$extras[$key] = "";                        
                    }
                    $index++;
                }
            }    
        }
	}
      
    public function getPost($name) {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }
      
    public function get($name) {
        return isset($_GET[$name]) ? $_GET[$name] : null;
    }
     
	   public function getID($ID) {
        return isset($_GET[$ID]) ? $_GET[$ID] : null;
    }
	
	
	public function getURL() {
        return $_SERVER[REQUEST_URI];
    }

    public function getFile($name){
        return isset($_FILES[$name]) ? $_FILES[$name] : null;
    }
	
    public function redirectTo($url) {
        header('Location: ' . $url);
        exit (0);
    }
     
    public function isPostRequest() {
        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : "";
        return $method == "POST" ? true : false;
    }
     
     public function getRoot() {
         global $ROOT;
         return $ROOT;
     }
	
	public abstract function create($extras);
	public abstract function render();
 }
?>