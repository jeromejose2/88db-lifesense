$(document).ready(function() {
    if ($(document).has("user_category_filter")) {
        $("#user_category_filter").change(function() {
            window.location.href=$("#url").val() + $(this).val();
        });
    }
    
    if ($(document).has("changepass")) {
        $("#changepass").click(function() {
            window.location.href=$("#changepass").val();
        });
    }
    
    if ($(document).has("updateinfo")) {
        $("#updateinfo").click(function() {
            window.location.href=$("#updateinfo").val();
        });
    }    

    if ($(document).has("editaccount")) {
        $("#editaccount").click(function() {
            window.location.href=$("#editaccount").val();
        });
    }    
    
    if ($(document).has("view_cart")) {
        $("#view_cart").click(function() {
            window.location.href=$("#view_cart").val();
        });
    }
    
    if ($(document).has("continue_shopping")) {
        $("#continue_shopping").click(function() {
            window.location.href=$("#continue_shopping").val();
        });
    }
    
    if ($(document).has("checkout")) {
        $("#checkout").click(function() {
            window.location.href=$("#checkout").val();
        });
    }    
        
    if ($(document).has("addtocart")) {
        $("#addtocart").click(function() {
            if ($("#qty").val() > 0) {
                window.location.href=$("#addtocart").attr("rel");
            }
        });
    }
    
    if ($(document).has("signup_form")) {
        $("#signup_form").validate();
    }    
});

$(window).load(function() {
});

$('.navigation ul#sub-nav li').hover(function() {
    var sublet = 'ul.' + this.id + '.sublets';				
    $(sublet).toggle();
});

$('ul#user-accordion-pane li a').click(function() {
    var accordion = 'div.' + this.id + '.accopen';
    var plus = 'ul li a#' + this.id; 
    $(accordion).toggle();
    $(plus).addClass('open');
    
    if ($('.'+this.id).is(':visible')) {
        $(plus).addClass('open');
    } else {
        $(plus).removeClass('open');
    }
    return false;							
});             
