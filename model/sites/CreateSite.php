<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class CreateSite extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        $extras = parent::getExtras();

        if (User::isAuthenticated($user_id))
        {
            $info = array(
                "pageTitle" => "Own Site",
                "last_login" => ""
            );

            if ($extras != null && count($extras) > 0)
            {
                if (isset($extras['action']))
                {
                    if ($extras['action'] == 'create')
                    {
                        $sitename = parent::getPost("sitename");
                        $siteUrl = parent::getPost("siteurl");
                        $siteDesc = parent::getPost("storedescription");
                        $structure = ROOT . "client/test";
                        $coredir = dirname(__FILE__) . "/";
                        $basepath = realpath($coredir . "../../") . "/";
                        $structure = $basepath . "client/" . $siteUrl;
                        if (!file_exists($structure))
                        {
                            if (!mkdir("$structure", 0777, true))
                            {
                                die('Failed to create folders...');
                            }
                            else
                            {
                                GUI::render("sites/createsite.php", $info);
                            }
                        }
                        else
                        {
                            $path = ROOT."members/home";
                            echo "You already have the site";
                            echo "<script>setTimeout(\"location.href = '$path';\",1000);</script>";
                        }
                    }
                }
            }
            else
            {
                $result = Db::execute("SELECT rm.lastname as last_name, rm.firstname as first_name, ru.id,rm.phone_no as phone,
                    CONCAT(rm.shipping_street,',',rm.shipping_state,' ',rm.shipping_country,' ',rm.shipping_zipcode) as shipping_street_addr,rm.shipping_city,ru.email
                    FROM `rewards_users` AS ru 
                     INNER JOIN `rewards_members` AS rm ON ru.`user` = rm.`id` 
                     WHERE ru.`id` = '$user_id' LIMIT 0,1");

                $pad = str_pad($user_id, 11, "0", STR_PAD_LEFT);
                $image = Db::execute("SELECT image_directory FROM rewards_members WHERE id = $pad");

                if (count($result) > 0)
                {
                    $result = $result[0];
                    $login_result = Db::execute("SELECT ID, DATE_FORMAT(login_date,'%W, %M %e, %Y @ %h:%i %p') AS lastlogin FROM rewards_login_history WHERE user_id = $user_id ORDER BY ID DESC limit 0,1 ");
                    $info["last_login"] = date("F j, Y, g:i a");
                    $info["last_login"] = $login_result[0]["lastlogin"];
                    $info["member_name"] = $result["first_name"] . ' ' . $result["last_name"];
                    $info["member_websitename"] = strtolower(trim($result["first_name"] . '' . $result["last_name"])) . ".lifesense.com";

                    $info["member_id"] = str_pad($result["id"], 11, "0", STR_PAD_LEFT);
                    $info["landline_no"] = $result["phone"];
                    $info["address"] = $result["shipping_street_addr"] . ",\n" . $result["shipping_city"]; //. ",\n" . $result["country"];
                    $info["email"] = $result["email"];
                    $info["image"] = $image[0]["image_directory"];
                }

                GUI::render("sites/createsite.php", $info);
            }
        }
        else
        {
            GUI::render("home.tpl.php", array("pageTitle" => "LifeSense",
                "announce_title" => "Company Announcement Title",
                "announce_content" => "Company Announcement Content"));
        }
    }

}

?>