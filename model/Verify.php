<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Verify extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        $extras = self::getExtras();
        $ac = $extras["ac"];
        $code = $ac;        
        if ($code != "") {
            $result = Db::query(Table::ACTIVATION_CODES,array("id","activation_code","activated","member_id"),
                                                        array("activation_code" => Db::quote($code)),"0,1");
            if (count($result) > 0) {
                $result = $result[0];
                $member_id = $result["member_id"];
//               $member_id =  str_pad($member_id,11,"0",STR_PAD_LEFT);
                if ($result["activated"] == 0) {
                    Db::update(Table::ACTIVATION_CODES,array("activated" => 1,"date_activated" => "NOW()"),array("id" => $result["id"]));
                    Db::update(Table::MEMBERS,array("status" => User::STATUS_ACTIVE),array("id" => $result["member_id"]));
                    Db::update(Table::USERS,array("status" => User::STATUS_ACTIVE),array("user"=>$result["member_id"]));
                }
                $member_id =  str_pad($member_id,11,"0",STR_PAD_LEFT);
                $result = Db::execute("SELECT ru.`id`, ru.`role` FROM `rewards_users` AS ru WHERE ru.user = '$member_id' LIMIT 0,1");
                if (is_array($result) && count($result) > 0) {
                    $result = $result[0];
                    
                    Session::start();
                    Session::register("user_id",$result["id"]);
                    Session::register("role",$result["role"]);
                    parent::redirectTo(ROOT . "members/home");
                } else {
                    parent::redirectTo(ROOT . "home/action/verified");
                }
            }
        } else {
            parent::redirectTo(ROOT . "home");
        }
	}
 }
?>