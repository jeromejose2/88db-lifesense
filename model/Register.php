<?php
 require_once 'core/Model.php';
 
 class Register extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        // P1 = SP 0.2
        // starter kit P1,700
        
        $email = parent::getPost("email");
        $firstname = parent::getPost("firstname");
        $lastname = parent::getPost("lastname");
        $sponsor_id = parent::getPost("sponsor");

        $code = self::generateCode();
        $pass = self::generatePass();
        
        $member_id = Db::insert(Table::MEMBERS,array("email" => Db::quote(parent::getPost("email")),
                                        "firstname" => Db::quote(parent::getPost("firstname")),
                                        "lastname" => Db::quote(parent::getPost("lastname")),
                                        "mi" => Db::quote(parent::getPost("mi")),
                                        "phone_number" => Db::quote(parent::getPost("phone")),
                                        "mobile_number" => Db::quote(parent::getPost("mobile")),
                                        "street" => Db::quote(parent::getPost("street")),
                                        "city" => Db::quote(parent::getPost("city")),
                                        "country" => Db::quote(parent::getPost("country")),
                                        "gender" => Db::quote(parent::getPost("gender")),
                                        "email" => Db::quote(parent::getPost("email")),
                                        "birthdate" => "STR_TO_DATE(".Db::quote(parent::getPost("birthdate")).",'%m/%d/%Y')",
                                        "marital_status" => Db::quote(parent::getPost("marital_status")),
                                        "sponsor" => Db::quote(parent::getPost("sponsor")),
                                        "creation_date" => "NOW()",
                                        "passport" => Db::quote(parent::getPost("passport")),
                                        "drivers_license" => Db::quote(parent::getPost("drivers_license")),
                                        "tin" => Db::quote(parent::getPost("tin")),
                                        "office_street" => Db::quote(parent::getPost("office_street")),
                                        "office_city" => Db::quote(parent::getPost("office_city")),
                                        "office_country" => Db::quote(parent::getPost("office_country")),
                                        "shipping_street" => Db::quote(parent::getPost("shipping_street")),
                                        "shipping_city" => Db::quote(parent::getPost("shipping_city")),
                                        "shipping_country" => Db::quote(parent::getPost("shipping_country")),
                                        "shipping_zipcode" => Db::quote(parent::getPost("shipping_zipcode")),
                                        "activation_code" => Db::quote($code)
                                       ),null,null);

        Db::insert(Table::USERS,array("user" => Db::quote(str_pad($member_id,11,"0",STR_PAD_LEFT)),
                                      "email"=> Db::quote($email),
                                      "pass" => Db::quote(User::encodePass($email,$pass)),
                                      "role" => User::USER_ROLE_MEMBER,
                                      "fullname" => Db::quote($firstname . " " . $lastname),
                                      "creation_date" => "NOW()"),null,null);
        
        Db::insert(Table::ACTIVATION_CODES,array("member_id" => $member_id,
                                                 "activation_code" => Db::quote($code),
                                                 "date_generated" => "NOW()",
                                                 "activated" => "0"),null,null);
                
        $result = Db::query(Table::MEMBERS,array("sponsor"),array("id" => $member_id),"0,1");
        
        if (count($result) > 0) {
            $result = $result[0];
            $sponsor_id = $result["sponsor"];

            Db::insert(Table::DOWNLINES,array("parent" => $sponsor_id,
                                              "child" => $member_id));
        }

        $to      = $email;
        $subject = "Congratulations for joining LifeSense!";
        $message = "Hi " . $firstname . " " . $lastname."\r\n";
        $message.= "Thanks for signing up! Please use the following email & password to log in:\r\n";
        $message.= "\r\n";
        $message.= "Email: $email\n";
        $message.= "Your temporary password is: $pass\r\n";
        $message.= "Your member id is: " . str_pad($member_id,11,"0",STR_PAD_LEFT) . "\r\n";
        $message.= "\r\n";
        $message.= "Activate your account by clicking the link below:\r\n";
        $message.= ROOT . "verify/ac/" . base64_encode(str_pad($member_id,11,"0",STR_PAD_LEFT).$code) ."\r\n";
        $headers = 'From: LifeSense <webmaster@lifesense.ph>' . "\r\n" .
                   'Reply-To: webmail@lifesense.ph' . "\r\n";
        mail($to,$subject,$message,$headers);
        
        parent::redirectTo(ROOT . "verify-account/member_id/$member_id");
    }
     
    public function generateCode() {
        $table = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($table);
        $code = "";
        $index = 0;
        while ($index < 10) {
            $n = rand(0,$size);
            $code.=$table{$n};
            $index++;
        }
        return $code;
    }
     
    public function generatePass() {
        $table = "abcdefghijklmnopqrstuvwxyz$@!ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($table);
        $pass = "";
        $index = 0;
        while ($index < 10) {
            $n = rand(0,$size);
            $pass.=$table{$n};
            $index++;
        }
        return $pass;
    }
 }
?>