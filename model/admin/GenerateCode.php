<?php

/*
 * Created By Jerome.J 04-04-2014
 */
require_once 'core/Model.php';
require_once 'core/Gui.php';

class GenerateCode extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN))
        {
            $info = array(
                "pageTitle" => "Generate Code",
                "root" => ROOT
            );
            $vchr_CharsToUse = "ABCDEFGHJKMNPQRSTUVWXYZ0123456789";
            $int_RandAlphaLength = STRLEN($vchr_CharsToUse);

            $query = "SELECT count(id) as countActivated FROM rewards_registration_code where activated = 1";
            $countActivated1 = Db::db_execute(0, $query);
            $countActivated = $countActivated1[0]['countActivated'];
            $query = "SELECT count(id) as countALL FROM rewards_registration_code";
            $countALL1 = Db::db_execute(0, $query);
            $countALL2 = $countALL1[0]['countALL'];
            $countALL = ($countALL2 / 2);
            $countALL = floor($countALL);
            $CountGenerated = 100;
            if ($countActivated >= $countALL)
            {
                for ($x = 0; $x < $CountGenerated; $x++)
                {
                    for ($p = 0; $p < 10; $p ++)
                    {
                        $code .= ($p % 2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))];
                    }
                    $code = preg_replace('/\s+/', ' ', $code);
                    $code = trim($code);
                    var_dump($code);

                    $query = "INSERT INTO rewards_registration_code(activation_code) VALUES ('$code')";
                    $insert = Db::db_execute(0, $query);
                }
                if ($x == $CountGenerated)
                {
                    $path = ROOT . "admin/home";
                    echo "Done generating 1000 activation codes";
                    echo "<script>setTimeout(\"location.href = '$path';\",1000);</script>";
                }
            }
            else
            {
                $path = ROOT . "admin/home";
                echo "There are more not activated codes than the activate ones";
                echo "<script>setTimeout(\"location.href = '$path';\",1000);</script>";
            }


            GUI::render("admin/memberinfo.tpl.php", $info);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

}
