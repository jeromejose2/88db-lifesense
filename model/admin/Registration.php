<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';
require_once 'core/Pagination.php';

class Registration extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN))
        {
            $months = self::getMonths();
            $days = self::getDays();
            $years = self::getYears();
            GUI::render("admin/registration.tpl.php", array('pageTitle' => 'LifeSense',
                'root' => ROOT,
                'months' => $months,
                'days' => $days,
                'years' => $years));
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

    public function getMonths()
    {
        $months = array(
            0 => array("id" => 1, "name" => "Jan"),
            1 => array("id" => 2, "name" => "Feb"),
            2 => array("id" => 3, "name" => "Mar"),
            3 => array("id" => 4, "name" => "Apr"),
            4 => array("id" => 5, "name" => "May"),
            5 => array("id" => 6, "name" => "Jun"),
            6 => array("id" => 7, "name" => "Jul"),
            7 => array("id" => 8, "name" => "Aug"),
            8 => array("id" => 9, "name" => "Sep"),
            9 => array("id" => 10, "name" => "Oct"),
            10 => array("id" => 11, "name" => "Nov"),
            11 => array("id" => 12, "name" => "Dec")
        );

        return $months;
    }

    public function getYears()
    {
        $years = array();

        $current_year = date("Y");
        $minimum_age = $current_year - 10;
        $maximum_age = $current_year - 100;
        $year = $minimum_age;

        while ($year > $maximum_age)
        {
            $years[] = $year;
            $year--;
        }
        return $years;
    }

    public function getDays()
    {
        $days = array();
        $day = 1;
        while ($day <= 31)
        {
            $days[] = $day;
            $day++;
        }
        return $days;
    }

}

?>