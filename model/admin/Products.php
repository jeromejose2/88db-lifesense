<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Products extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $extras = parent::getExtras();
            $category = "";
            $page = 1;

            if (count($extras) > 0 && isset($extras["type"]) && isset($extras["action"])) {
                if ($extras["type"] == "product") {
                    if ($extras["action"] == "view") {
                    } else if ($extras["action"] == "delete") {
                    } else if ($extras["action"] == "add") {
                        if (parent::isPostRequest()) {
                            $name = parent::getPost("name");
                            $description = parent::getPost("description");
                            $price = floatval(parent::getPost("price"));
                            $tax = floatval(parent::getPost("tax"));
                            $discount = floatval(parent::getPost("discount"));
                            $quantity = intval(parent::getPost("quantity"));
                            $product_type = intval(parent::getPost("product_type"));
                            $photo = "";
                            
                            $file = $_FILES["photo"];
                            if (in_array($file["type"],array("image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png"))) {
                                $fileSize = intval($file["size"]);
                                $fileExt = end(explode(".",$file["name"]));
                                if (in_array($fileExt,array("jpg","jpeg","png")) && $fileSize < 200000) {
                                    if ($file["error"] > 0) {
                                    } else {
                                        $photo = "images/products/" . md5($user_id . date("YmdHis")) . "." . $fileExt;
                                        move_uploaded_file($file["tmp_name"],$photo);
                                    }
                                } else {
                                }
                            }
                            
                            Db::insert(Table::PRODUCTS,array("in_stock" => $quantity,
                                                             "price" => $price,
                                                             "tax" => $tax,
                                                             "product_type" => $product_type,
                                                             "description" => Db::quote($description),
                                                             "name" => Db::quote($name),
                                                             "photo" => Db::quote($photo),
                                                             "discount" => $discount));
                            parent::redirectTo(ROOT . "admin/products");
                        } else {
                            $info = array("pageTitle" => "Products",
                                          "show_cart_items" => "true",
                                          "root" => ROOT);

                            $product_types = self::getProductTypes("");
                            $info["product_types"] = $product_types;
                            GUI::render("admin/add_new_product.tpl.php",$info);                    
                        }                        
                    }
                } else if ($extras["type"] == "category") {
                    if ($extras["action"] == "view") {
                    } else if ($extras["action"] == "delete") {
                    } else if ($extras["action"] == "add") {
                        if (parent::isPostRequest()) {
                            $title = parent::getPost("name");
                            Db::insert(Table::PRODUCT_TYPES,array("title" => Db::quote($title),"parent" => "0"));
                            parent::redirectTo(ROOT . "admin/products/type/category/action/manage");                            
                        } else {
                            $info = array("pageTitle" => "Products",
                                          "show_cart_items" => "true",
                                          "root" => ROOT);
    
                            GUI::render("admin/add_new_category.tpl.php",$info);
                        }
                    } else {
                        $info = array("pageTitle" => "Products",
                                      "show_cart_items" => "true",
                                      "root" => ROOT);
                        
                        $product_types = self::getProductTypes("");
                        $info["categories"] = $product_types;

                        GUI::render("admin/manage_product_category.tpl.php",$info);                                                         
                    }
                }
            } else {
                if (count($extras) > 0) {
                    if (isset($extras["category"])) {
                        $category = $extras["category"];
                    }
                    
                    if (isset($extras["page"])) {
                        $page = intval($extras["page"]);
                        $page = $page < 1 ? 1 : $page;
                    }
                }
                self::display_products($page,$category);    
            }
                        
//            if (count($extras) > 0 && isset($extras["action"]) && in_array($extras["action"],array("add","delete","manage","view"))) {
//                if ($extras["action"] == "add") {
//                    if (parent::isPostRequest()) {
//                        $name = parent::getPost("name");
//                        $description = parent::getPost("description");
//                        $price = floatval(parent::getPost("price"));
//                        $tax = floatval(parent::getPost("tax"));
//                        $discount = floatval(parent::getPost("discount"));
//                        $quantity = intval(parent::getPost("quantity"));
//                        $product_type = intval(parent::getPost("product_type"));
//                        $photo = "";
//                        
//                        $file = $_FILES["photo"];
//                        if (in_array($file["type"],array("image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png"))) {
//                            $fileSize = intval($file["size"]);
//                            $fileExt = end(explode(".",$file["name"]));
//                            if (in_array($fileExt,array("jpg","jpeg","png")) && $fileSize < 200000) {
//                                if ($file["error"] > 0) {
//                                } else {
//                                    $photo = "images/products/" . md5($user_id . date("YmdHis")) . "." . $fileExt;
//                                    move_uploaded_file($file["tmp_name"],$photo);
//                                }
//                            } else {
//                            }
//                        }
//                        
//                        Db::insert(Table::PRODUCTS,array("in_stock" => $quantity,
//                                                         "price" => $price,
//                                                         "tax" => $tax,
//                                                         "product_type" => $product_type,
//                                                         "description" => Db::quote($description),
//                                                         "name" => Db::quote($name),
//                                                         "photo" => Db::quote($photo),
//                                                         "discount" => $discount));
//                        parent::redirectTo(ROOT . "admin/products");
//                    } else {
//                        $product_types = self::getProductTypes("");
//                        $info["product_types"] = $product_types;
//                        GUI::render("admin/add_new_product.tpl.php",$info);                    
//                    }
//                } else if ($extras["action"] == "manage") {
////                    if (count($extras) > 2 && isset($extras["add"])) {
////                        if (parent::isPostRequest()) {
////                            $title = parent::getPost("name");
////                            Db::insert(Table::PRODUCT_TYPES,array("title" => Db::quote($title),"parent" => "0"));
////                            parent::redirectTo(ROOT . "admin/products/manage/category");
////                        } else {
////                            GUI::render("admin/add_new_category.tpl.php",$info);
////                        }
////                    } else {
//                        $product_types = self::getProductTypes("");
//                        $info["categories"] = $product_types;
//
//                        GUI::render("admin/manage_product_category.tpl.php",$info);                                            
////                    }
//                } else {
//                    parent::redirectTo(ROOT . "admin/products");
//                }
//            } else {
//                $category = "";
//                if ($extras != null && count($extras) > 1) {
//                    $category = intval($extras[1]);
//                }
//                
//                $products = self::getProducts($category);
//                $product_types = self::getProductTypes($category);
//                
//                $page = 1;
//            
//                $pages = Pagination::calc($page,5,$products["total"]);
//
//                
//                $info["products"] = $products["products"];                
//                $info["product_types"] = $product_types;
//                $info["pages"]= $pages["pages"];
//                $info["current_page"] = $page;
//                $info["num_pages"] = $pages["total"];
//                
//                GUI::render("admin/products.tpl.php",$info);
//            }
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
     
    public function display_products($page,$category) {
        $products = self::getProducts($page,$category);
        $product_types = self::getProductTypes($category);
    
        $pages = Pagination::calc($page,5,$products["total"],5);
        
        $info = array("pageTitle" => "Products",
              "show_cart_items" => "true",
              "root" => ROOT);
        
        $info["products"] = $products["products"];                
        $info["product_types"] = $product_types;
        $info["pages"]= $pages["pages"];
        $info["current_page"] = $page;
        $info["num_pages"] = $pages["total"];        
            
        GUI::render("admin/products.tpl.php",$info);                    
    } 
    
    private function getProductTypes($category) {
        $product_types = array();
        if ($category != "") {
            $product_types = Db::query(Table::PRODUCT_TYPES,array("id","title"),array("id" => $category),null);
        } else {
            $product_types = Db::query(Table::PRODUCT_TYPES,array("id","title"),null,null);
        }
        return $product_types;
    }
     
    private function getProducts($page,$category) {
        $products = array();
        $total_products = 0;
        
        $result = Db::query(Table::PRODUCTS,
                              array("COUNT(id) as num_products"),
                              null,null);
        
        if (count($result) > 0) {
            $total_products = $result[0]["num_products"];
        }
        
        $result = Db::query(Table::PRODUCTS,
                              array("id","name","description","price","in_stock","discount","photo"),
                              null,"0,5");
        $itemCount = 0;
        if (count($result) > 0) {
            foreach ($result as $product) {
                $products[]=array(
                    "id" => $product["id"],
                    "name" => $product["name"],
                    "description" => $product["description"],
                    "price" => $product["price"],
                    "discount" => $product["discount"],
                    "photo" => $product["photo"],
                    "in_stock" => $product["in_stock"]
                );
                $itemCount++;
            }
        }
        
        while ($itemCount < 5) {
            $products[]=array(
                "id" => "",
                "name" => "",
                "description" => "",
                "price" => "",
                "discount" => "",
                "photo" => "",
                "in_stock" => ""
            );
            $itemCount++;            
        }
        return array("total" => $total_products,"products" => $products);
    }
 
 }
?>