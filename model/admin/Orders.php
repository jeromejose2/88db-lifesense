<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Orders extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $extras = parent::getExtras();
            $supressParent = false;
            if (count($extras) > 0) {
                if (isset($extras["action"])) {
                    $action = $extras["action"];
                    if ($action == "view" && isset($extras["type"])) {
                        $type = $extras["type"];
                        if ($type == "detail") {
                            self::displayOrderDetail(intval($extras["orderid"]));
                            $supressParent = true;
                        } else if ($type == "invoice") {
                            self::displayOrderInvoice(intval($extras["orderid"]));
                            $supressParent = true;
                        }
                    } else if ($action == "confirm") {
                        self::confirmOrder(intval($extras["orderid"]));
                        $supressParent = true;
                    }
                }
            }
            if (!$supressParent) {
                self::displayOrderList($extras);
            }    
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
                                           
    public function confirmOrder($order_id) {
        print $order_id;
        Db::execute("UPDATE rewards_orders SET process_date = NOW(), status = '3' WHERE id = '$order_id'");
        parent::redirectTo(ROOT . "admin/orders");
    }
     
    public function displayOrderList($extras) {
        $info = array("pageTitle" => "Orders",
                      "show_cart_items" => "true",
                      "root" => ROOT);            
        $total_orders = 0;
        $num_pages = 1;
        
        $page = 1;
        
        if (count($extras) > 0) {
            if (isset($extras["page"])) {
                 $page = intval($extras["page"]);
                $page = $page < 1 ? 1 : $page;
            } 
        }
        
        $total_orders = self::getTotalOrders();
        
        $limit = 10;
        $offset = $limit * ($page-1);
        
        $orders = self::getOrders($offset,$limit);
        $info["orders"] = $orders;            

        $pages = Pagination::calc($page,5,$total_orders,$limit);
        
        $info["num_pages"] = $pages["total"];
        $info["current_page"] = $page;
        $info["pages"] = $pages["pages"];
        $info["first"] = $pages["first"];
        $info["prev"] = $pages["prev"];
        $info["next"] = $pages["next"];
        $info["last"] = $pages["last"];            

        GUI::render("admin/orders.tpl.php",$info);    
    }
     
    public function displayOrderDetail($orderid) {
        $info = array("pageTitle" => "Order Detail",
                      "show_cart_items" => "true",
                      "root" => ROOT);
        
        $result = self::getOrderDetail($orderid);
        $info["orders"] = $result;
        $info["total"] = $result[0]["total"];
        $result = self::getOrderItems($orderid);
        $info["order_items"] = $result;
        
        GUI::render("admin/order_detail.tpl.php",$info);    
    }
     
    public function displayOrderInvoice($order_id) {
        $result = Db::execute("SELECT invoice FROM rewards_order_invoice WHERE order_id='$order_id'");
        if (count($result) > 0) {
            $invoice = $result[0]["invoice"];
            header("Content-type: application/pdf");
            
            print file_get_contents (getcwd() . "/invoice/$invoice.pdf");
        } else {
            print "No Invoice";
        }        
    }
     
    public function getOrderItems($order_id) {
//        $result= Db::db_execute(USE_SPHERE_DB,"SELECT soi.id, soi.quantity, FORMAT(soi.price,2) AS price,sp.name FROM shop_order_items AS soi INNER JOIN shop_orders AS so ON soi.shop_order_id = so.id INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id WHERE so.id = '$order_id'");
        $result= Db::db_execute(0,"SELECT soi.id, soi.quantity, FORMAT(soi.price,2) AS price,sp.name FROM shop_order_items AS soi INNER JOIN shop_orders AS so ON soi.shop_order_id = so.id INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id WHERE so.id = '$order_id'");
        return $result;
    } 
     
    public function getOrderDetail($order_id) {
        $result = Db::db_execute(0,"SELECT so.*,CONCAT(sc.first_name,' ',sc.last_name) AS customer_name,sn.name AS billing_country,ss.name AS billing_state,sn2.name AS shipping_country, ss2.name AS shipping_state FROM shop_orders AS so INNER JOIN shop_customers AS sc ON so.customer_id = sc.id INNER JOIN shop_countries AS sn ON so.billing_country_id = sn.id INNER JOIN shop_states AS ss ON so.billing_state_id = ss.id INNER JOIN shop_states AS ss2 ON so.shipping_state_id = ss2.id INNER JOIN shop_countries AS sn2 ON so.shipping_country_id = sn2.id WHERE so.id='$order_id'");
        return $result;
    } 
    
    public function getOrders($offset,$limit) {
        $result = Db::db_execute(0,"SELECT so.id,CONCAT(sc.first_name,' ',sc.last_name) AS customer_name,sc.email,so.order_datetime,so.total,so.subtotal,so.discount,ss.name AS status FROM shop_orders AS so INNER JOIN shop_customers AS sc ON so.customer_id = sc.id INNER JOIN shop_order_statuses AS ss ON so.status_id = ss.id ORDER BY so.order_datetime DESC LIMIT $offset,$limit");
        return $result;
    } 
     
    public function getTotalOrders() {
        $result = Db::db_execute(0,"SELECT COUNT(id) as total FROM shop_orders");
        return $result[0]["total"];
    }     
 }

class InvoicePDF extends FPDF {
    function Header() {
        $this->Image('images/sphere/mlm-logo.png',10,6,30);
        $this->SetFont('Arial','B',10);
        $this->Cell(80);
        $this->Cell(30,10,'Sphere Rewards Invoice',0,1,'C');
        $this->Ln(30);
    }
    
    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
?>