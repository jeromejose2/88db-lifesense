<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Home extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();
        //echo "render";
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN))
        {
            $info = array(
                "pageTitle" => "Home",
                "root" => ROOT,
                "name" => "",
                "last_login" => ""
            );

            $result = Db::query(Table::USERS, array("id", "fullname"), array("id" => $user_id), "0,1");
            if (count($result) > 0)
            {
                $result = $result[0];
                $info["name"] = $result["fullname"];
            }

            $login_result = Db::execute("SELECT DATE_FORMAT(login_date,'%W, %M %e, %Y @ %h:%i %p') AS lastlogin FROM rewards_login_history ORDER BY login_date desc LIMIT 0,2");
            $info["last_login"] = date("F j, Y, g:i a");
            if (count($login_result) > 1)
            {
                $info["last_login"] = $login_result[1]["lastlogin"];
            }

            $members = array();
            $result = Db::query(Table::MEMBERS, array("firstname", "lastname", "sponsor", "creation_date"), null, "0,5", array("id"), 1);
            $itemCount = 0;
            if (count($result) > 0)
            {
                foreach ($result as $item)
                {
                    $members[] = array(
                        "name" => $item["firstname"] . " " . $item["lastname"],
                        "sponsor" => self::getSponsor($item["sponsor"]),
                        "date" => $item["creation_date"]
                    );
                    $itemCount++;
                }
            }

            while ($itemCount < 5)
            {
                $members[] = array(
                    "name" => "",
                    "sponsor" => "",
                    "date" => ""
                );
                $itemCount++;
            }

            $result = Db::query(Table::USERS, array("COUNT(id) num_users"), null, null);
            if (count($result) > 0)
            {
                $info["num_users"] = $result[0]["num_users"];
            }

            $result = Db::query(Table::USERS, array("COUNT(id) num_users"), array("status" => User::STATUS_NEW), null);
            if (count($result) > 0)
            {
                $info["new_users"] = $result[0]["num_users"];
            }

            $result = Db::query(Table::MEMBERS, array("COUNT(id) num_members"), null, null);
            if (count($result) > 0)
            {
                $info["num_members"] = $result[0]["num_members"];
            }
            $result = Db::query(Table::MEMBERS, array("COUNT(id) num_members"), array("status" => User::STATUS_NEW), null);
            if (count($result) > 0)
            {
                $info["new_members"] = $result[0]["num_members"];
            }

            $result = Db::query(Table::MEMBERS, array("COUNT(id) num_members"), array("activated" => 0), null);
            if (count($result) > 0)
            {
                $info["pending_members"] = $result[0]["num_members"];
            }


            $info["members"] = $members;


            /*
             * added by Jerome.J 04-03-2014
             */

            $weeklyperMonth = Db::execute("SELECT COUNT(id) as count FROM rewards_members 
                 WHERE YEAR(creation_date) = YEAR(CURRENT_DATE())  AND MONTH(creation_date) = MONTH(DATE_SUB(NOW(), INTERVAL 1 MONTH)) GROUP BY WEEK(creation_date);");
            $week = 0;
            while ($week < 5)
            {
//                $week_count[] = array($week => $weeklyperMonth[$week]['count']);
                if ($week == 0)
                {
                    $week_count[] = 0;
                }
                else
                {
                    $week_count[] = $weeklyperMonth[$week - 1]['count'];
                    $total_week_count += $weeklyperMonth[$week - 1]['count'];
                }

                $week++;
            }

            $result = Db::execute("SELECT rm.id,CONCAT(rm.firstname,' ',rm.lastname) AS name,rm.points FROM rewards_members AS rm
INNER JOIN rewards_member_commissions AS rc ON rm.id = rc.member_id
WHERE rc.`commission_date` = YEAR(CURRENT_DATE) OR MONTH(rc.`commission_date`) = MONTH(CURRENT_DATE)
GROUP BY name ORDER BY rm.`points` desc LIMIT 0,5");
            $count = count($result);
            if ($count > 0)
            {
                $info["top_5_earning_members"] = $result;
            }
            while ($count < 5)
            {
                $info["top_5_earning_members"][] = array(
                    "id" => "",
                    "name" => "",
                    "points" => ""
                );
                $count++;
            }

            $info["total_week_count"] = $total_week_count;
            $info["week_count"] = $week_count;
            self::displayGraph($info);

            GUI::render("admin/home.tpl.php", $info);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

    private function displayGraph($info)
    {
        require_once 'lib/jpgraph/jpgraph.php';
        require_once 'lib/jpgraph/jpgraph_line.php';
        require_once 'lib/jpgraph/jpgraph_pie.php';
        require_once 'lib/jpgraph/jpgraph_pie3d.php';
        require_once 'lib/jpgraph/jpgraph_bar.php';

//        $ydata = array(11, 3, 8, 12, 5, 1, 9, 13, 5, 7);
//        $ydata1 = array(1, 3, 5, 10, 15, 1, 9, 13, 5, 7);
//        $weeklycount = $info['week_count'];
//        $width = 650;
//        $height = 250;
//
//        $graph = new Graph($width, $height);
//        $graph->SetScale('intlin');
////        $graph->SetScale("textlin");
//        $theme_class = new UniversalTheme();
//
//        $graph->SetTheme($theme_class);
//
//        $label = Db::execute("SELECT CONCAT(MONTH(DATE_SUB(NOW(), INTERVAL 1 MONTH)),'/',DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)),'/',YEAR(DATE_SUB(NOW(), INTERVAL 1 MONTH)))AS lastmonth,CURRENT_DATE()AS monthtoday;");
//        $lastmonth = date("M-d-y", strtotime($label[0]['monthtoday']));
//        $monthtoday = date("M-d-y", strtotime($label[0]['lastmonth']));
//        $graph->SetMargin(40, 20, 20, 40);
//
//        $graph->title->Set('LifeSense Members Per Month');
////        $graph->subtitle->Set('(' . $lastmonth . ' to ' . $monthtoday . ')');
//        $graph->xaxis->title->Set('Weeks  (' . $lastmonth . ' to ' . $monthtoday . ')');
//        $graph->yaxis->title->Set('# of members');
//
//        $graph->img->SetAntiAliasing(false);
//        $graph->SetBox(false);
//        $graph->yaxis->HideZeroLabel();
//        $graph->yaxis->HideLine(false);
//        $graph->yaxis->HideTicks(false, false);
//        $graph->xaxis->SetTickSide(SIDE_BOTTOM);
//        $graph->legend->SetFrameWeight(1);
//
//
//        $lineplot = new LinePlot($weeklycount, array(0, 1, 2, 3, 4));
//
//        $graph->Add($lineplot);
//        unlink('images/members-line.png');
//        $graph->Stroke('images/members-line.png');
        // pie graph
//        $data = array(40, 60, 21, 33);
//
//        $graph = new PieGraph(300, 200);
//        $graph->SetShadow();
//
//        $graph->title->Set("A simple Pie plot");
//        $graph->title->SetFont(FF_FONT1, FS_BOLD);
//
//        $p1 = new PiePlot3D($data);
//        $p1->SetSize(0.5);
//        $p1->SetCenter(0.45);
//        $p1->SetLegends($gDateLocale->GetShortMonth());
//
//        $graph->Add($p1);
//        unlink('images/members-pie.png');
//        $graph->Stroke('images/members-pie.png');
//        $graph->img->SetMargin(40, 30, 40, 40);
        $top5 = $info["top_5_earning_members"];
        foreach ($top5 as $value)
        {
            $name[] = $value['name'];
            $points[] = $value['points'];
            $totalpoints += $value['points'];
//            $points[] = intval($value['points']);
        }

        $graph = new Graph(650, 450);
        $graph->SetScale("intlin", 0, $aYMax = ($totalpoints / 2.5));
        $theme_class = new UniversalTheme;
        $graph->SetTheme($theme_class);

        $graph->SetBox(false);

        $graph->ygrid->Show(true);
        $graph->xgrid->Show(false);
        $graph->yaxis->HideZeroLabel();
        $graph->xaxis->SetTickLabels($name);

        $txt = new Text('This is a text');
        $txt->SetPos(0, 20);
        $txt->SetColor('darkred');
        $txt->SetFont(FF_FONT2, FS_BOLD);
        $graph->AddText($txt);

// Create the line
        $p1 = new LinePlot($points);
        $graph->Add($p1);
        
        $txt = new Text('Points');
        $txt->SetPos(145, 250);
        $txt->SetColor('darkred');
        $txt->SetFont(FF_FONT1, FS_BOLD);
        $graph->AddText($txt);
        $txt = new Text('Members');
        $txt->SetPos(155, 480);
        $txt->SetColor('darkred');
        $txt->SetFont(FF_FONT1, FS_BOLD);
        $graph->AddText($txt);

        $p1->SetColor('blue');
        // Setup the titles
        $graph->title->Set("Life Sense Top Points");
//        $graph->xaxis->title->Set("Members");
//        $graph->yaxis->title->Set("Points");
        $graph->yaxis->SetLabelAngle(45);
        $graph->Set90AndMargin();
        $graph->title->SetFont(FF_FONT2, FS_BOLD);
        $graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
        $graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);
        // Display the graph.
        // Create and add a new text

        unlink('images/members-bar.png');
        $graph->Stroke('images/members-bar.png');
    }

    private function getSponsor($sponsor)
    {
        $result = Db::query(Table::MEMBERS, array("firstname", "lastname"), array("id" => $sponsor), "0,1");
        if (count($result) > 0)
        {
            return $result[0]["firstname"] . " " . $result[0]["lastname"];
        }
        else
        {
            return "";
        }
    }

    private function status()
    {
        echo "This";
    }

}

?>