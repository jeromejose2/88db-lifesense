<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Purchase extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $products = self::getProducts();        
            GUI::render("admin_purchase.tpl.php",array('pageTitle' => 'Purchase',
                                             'show_cart_items' => 'true',
                                             'root' => '../',
                                             'products' => $products
                                             ));
        } else {
            parent::redirectTo("../logout");
        }
	}
     
    private function getProducts() {
        $products = array();
        $result = Db::query(Table::PRODUCTS,
                              array("product_id","name","description","price","status","image","date_added"),
                              array("status"=>"active"),null);
        if (count($result) > 0) {
            foreach ($result as $product) {
                $products[]=array(
                    "product_id" => $product["product_id"],
                    "name" => $product["name"],
                    "description" => $product["description"],
                    "price" => $product["price"],
                    "status" => $product["status"],
                    "image" => $product["image"],
                    "date_added" => $product["date_added"]
                );
            }
        }
        return $products;
    }
 
 }
?>