<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Member_Genealogy extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        Session::start();
        $extras = self::getExtras();
        $member_id = -1;
        if (isset($extras["member_id"])) {
            $member_id=intval($extras["member_id"]);
        }
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Member Geneology",
                "root" => ROOT,
            );
            
            $members = array();
            
            $result = Db::query(Table::DOWNLINES,array("parent","child"),array("parent" => $member_id),"0,5");
            
            $level = 1;            
            if (count($result) > 0) {
                foreach ($result as $item) {
                    $sub_info=array(
                        "level" => $level,
                        "name" => "",
                        "date" => "",
                        "points" => "",
                        "status" => ""
                    );
                    $sub_result = Db::query(Table::MEMBERS,array("firstname","lastname","creation_date","points"),
                                                            array("id" => $item["child"]),"0,1");
                    if (count($sub_result) > 0) {
                        $i = $sub_result[0];
                        $sub_info["name"] = $i["firstname"] . " " . $i["lastname"];
                        $sub_info["date"] = $i["creation_date"];
                        
                        $commission = Db::execute("SELECT SUM(points) AS points FROM rewards_member_commissions WHERE member_id = '".$item["child"]."'");
                        $sp = 0;
                        if (count($commission) > 0) {
                            $sp = $commission[0]["points"];
                        }                        
                        $sub_info["points"] = $sp . " SP";
                    }
                    
                    $members[] = $sub_info;

                    $level++;
                }
            }
            
            while ($level <= 5+1) {
                $members[] = array(
                    "level" => "",
                    "name" => "",
                    "date" => "",
                    "points" => "",
                    "status" => ""
                );
                $level++;
            }
            
            $info["member_id"] = str_pad($member_id,11,"0",STR_PAD_LEFT);
            
            $info["members"] = $members;
            $info["num_pages"] = 1;
            $info["current_page"] = 1;
            
            GUI::render("admin/member_genealogy.tpl.php",$info);

        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>