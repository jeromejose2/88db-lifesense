<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';
require_once 'core/Pagination.php';

class Rewards_Reports extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN))
        {

            $extras = parent::getExtras();
            if ($extras != null && count($extras) > 0)
            {
                if ($extras["get"] == "memberlist")
                {
                    self::downloadMemberList($extras["as"]);
                }
                else if ($extras["get"] == "productlist")
                {
                    self::downloadProductList($extras["as"]);
                }
                else if ($extras["get"] == "orderlist")
                {
                    self::downloadOrderList($extras["as"]);
                }
            }
            else
            {
                self::displayReport();
            }
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

    public function downloadOrderList($type)
    {
        if ($type == "pdf")
        {
            $result = Db::execute("SELECT ro.id,ro.total_price,ro.member_id,ro.total_items,ro.purchased_on,ri.invoice FROM rewards_orders AS ro INNER JOIN rewards_order_invoice AS ri ON ro.id = ri.order_id ORDER BY ro.id");

//            $result = Db::db_execute(USE_SPHERE_DB, "SELECT so.id,so.total AS total_price,SUM(soi.quantity) AS total_items,so.order_datetime FROM shop_orders AS so INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id GROUP BY so.id ORDER BY so.order_datetime DESC");
            $pdf = new OrderReportPDF();
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Cell(30, 7, "ID", 1, 0, 'C');
            $pdf->Cell(30, 7, "Price", 1, 0, 'C');
            $pdf->Cell(30, 7, "Quantity", 1, 0, 'C');
            $pdf->Cell(60, 7, "Date", 1, 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 12);

            if (count($result) > 0)
            {
                foreach ($result as $order)
                {
                    $pdf->Cell(30, 6, $order["id"], 'LR');
                    $pdf->Cell(30, 6, $order["total_price"], 'LR');
                    $pdf->Cell(30, 6, $order["total_items"], 'LR');
                    $pdf->Cell(60, 6, $order["purchased_on"], 'LR');
                    $pdf->Ln();
                }
                $pdf->Cell(30 + 30 + 30 + 60 + 40, 0, '', 'T');
            }
            $pdf->Output();
        }
    }

    public function downloadMemberList($type)
    {
        $result = Db::execute("SELECT id,firstname,lastname,email,phone_no FROM rewards_members ORDER BY id");
        if ($type == "xls")
        {
            
        }
        else if ($type == "pdf")
        {
            $pdf = new MemberListReportPDF();
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Cell(30, 7, "ID", 1, 0, 'C');
            $pdf->Cell(30, 7, "First Name", 1, 0, 'C');
            $pdf->Cell(30, 7, "Last Name", 1, 0, 'C');
            $pdf->Cell(60, 7, "E-mail", 1, 0, 'C');
            $pdf->Cell(40, 7, "Tel. No", 1, 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 12);

            if (count($result) > 0)
            {
                foreach ($result as $member)
                {
                    $pdf->Cell(30, 6, $member["id"], 'LR');
                    $pdf->Cell(30, 6, $member["firstname"], 'LR');
                    $pdf->Cell(30, 6, $member["lastname"], 'LR');
                    $pdf->Cell(60, 6, $member["email"], 'LR');
                    $pdf->Cell(40, 6, $member["phone_no"], 'LR');
                    $pdf->Ln();
                }
                $pdf->Cell(30 + 30 + 30 + 60 + 40, 0, '', 'T');
            }
            $pdf->Output();
        }

        $result = null;
    }

    public function downloadProductList($type)
    {
        $result = Db::query(Table::PRODUCTS, array("id", "name", "price", "in_stock"), null, null);
        if ($type == "xls")
        {
            
        }
        else if ($type == "pdf")
        {
            $pdf = new ProductListReportPDF();
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Cell(24, 7, "Product ID", 1, 0, 'C');
            $pdf->Cell(124, 7, "Product Name", 1, 0, 'C');
            $pdf->Cell(20, 7, "Price", 1, 0, 'C');
            $pdf->Cell(20, 7, "In Stock", 1, 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 12);
            if (count($result) > 0)
            {
                foreach ($result as $member)
                {
                    $pdf->Cell(24, 6, $member["id"], 'LR');
                    $pdf->Cell(124, 6, $member["name"], 'LR');
                    $pdf->Cell(20, 6, $member["price"], 'LR');
                    $pdf->Cell(20, 6, $member["in_stock"], 'LR');
                    $pdf->Ln();
                }
                $pdf->Cell(24 + 124 + 20 + 20, 0, '', 'T');
            }
            $pdf->Output();
        }

        $result = null;
    }

    public function displayReport()
    {
        $info = array(
            "pageTitle" => "LifeSense Reports",
            "root" => ROOT,
        );

        $info["num_pages"] = 1;
        $info["current_page"] = 1;

        $total_members = 0;
        $total_member_this_month = 0;
        $total_products = 0;

        $result = Db::query(Table::MEMBERS, array("COUNT(id) as total_members"), null, null);
        if (count($result) > 0)
        {
            $total_members = intval($result[0]["total_members"]);
        }

        $result = Db::execute("SELECT COUNT(id) as total_members FROM rewards_members WHERE YEAR(creation_date) = YEAR(CURRENT_DATE) AND MONTH(creation_date) = MONTH(CURRENT_DATE)");
        if (count($result) > 0)
        {
            $total_member_this_month = intval($result[0]["total_members"]);
        }

        $result = Db::query(Table::PRODUCTS, array("COUNT(id) as total_products"), null, null);
        if (count($result) > 0)
        {
            $total_products = intval($result[0]["total_products"]);
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_ytd FROM rewards_orders WHERE status='3'");
        $total_sales_ytd = 0;
        if (count($result) > 0)
        {
            $total_sales_ytd = $result[0]["total_sales_ytd"] == "" ? 0 : $result[0]["total_sales_ytd"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_unpaid_ytd FROM rewards_orders WHERE status='1'");
        $total_sales_unpaid_ytd = 0;
        if (count($result) > 0)
        {
            $total_sales_unpaid_ytd = $result[0]["total_sales_unpaid_ytd"] == "" ? 0 : $result[0]["total_sales_unpaid_ytd"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_current FROM rewards_orders WHERE status='3' AND YEAR(purchased_on) = YEAR(CURRENT_DATE) AND MONTH(purchased_on) = MONTH(CURRENT_DATE)");
        $total_sales_current = 0;
        if (count($result) > 0)
        {
            $total_sales_current = $result[0]["total_sales_current"] == "" ? 0 : $result[0]["total_sales_current"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_unpaid_current FROM rewards_orders WHERE status='1' AND YEAR(purchased_on) = YEAR(CURRENT_DATE) AND MONTH(purchased_on) = MONTH(CURRENT_DATE)");
        $total_sales_unpaid_current = 0;
        if (count($result) > 0)
        {
            $total_sales_unpaid_current = $result[0]["total_sales_unpaid_current"] == "" ? 0 : $result[0]["total_sales_unpaid_current"];
        }

        $info["total_sales_current"] = $total_sales_current;
        $info["total_sales_unpaid_current"] = $total_sales_unpaid_current;

        $info["total_sales_ytd"] = $total_sales_ytd;
        $info["total_sales_unpaid_ytd"] = $total_sales_unpaid_ytd;

        $info["total_members"] = $total_members;
        $info["total_member_this_month"] = $total_member_this_month;
        $info["total_products"] = $total_products;

        $info["days"] = self::getDays();
        $info["months"] = self::getMonths();
        $info["years"] = self::getYears();

        $info["orders"] = self::getOrderList();

        $result = Db::execute("SELECT rm.id,CONCAT(rm.firstname,' ',rm.lastname) AS name,rm.points FROM rewards_members AS rm
INNER JOIN rewards_member_commissions AS rc ON rm.id = rc.member_id
WHERE rc.`commission_date` = YEAR(CURRENT_DATE) OR MONTH(rc.`commission_date`) = MONTH(CURRENT_DATE)
GROUP BY name ORDER BY rm.`points` desc LIMIT 0,10");
        $count = count($result);
        if ($count > 0)
        {
            $info["top_10_earning_members"] = $result;
        }
        while ($count < 10)
        {
            $info["top_10_earning_members"][] = array(
                "id" => "",
                "name" => "",
                "points" => ""
            );
            $count++;
        }

        $info["current_month"] = date("F");

        $result = Db::execute("SELECT (WEEK(purchased_on,5) - WEEK(DATE_SUB(purchased_on, INTERVAL DAYOFMONTH(purchased_on)-1 DAY),5)+1) AS purchase_week, SUM(total_price) AS total_price, SUM(total_items) "
                        . "AS total_items FROM rewards_orders WHERE YEAR(purchased_on) = YEAR(CURRENT_DATE) "
                        . "AND MONTH(purchased_on) = MONTH(CURRENT_DATE) GROUP BY WEEK(purchased_on) ORDER BY purchased_on ASC");

//        $result = Db::db_execute(USE_SPHERE_DB,"SELECT (WEEK(so.order_datetime,5) - WEEK(DATE_SUB(so.order_datetime, INTERVAL DAYOFMONTH(so.order_datetime) - 1 DAY), 5) + 1) AS purchase_week, SUM(so.total) AS total_price, SUM(soi.quantity) AS total_items FROM shop_orders AS so
//INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id WHERE YEAR(so.order_datetime) = YEAR(CURRENT_DATE) AND MONTH(so.order_datetime) = MONTH(CURRENT_DATE) GROUP BY WEEK(so.order_datetime) ORDER BY so.order_datetime ASC");
        if (count($result) > 0)
        {
            $current_month_sales = array();
            $week = 1;
            while ($week <= 5)
            {
                $current_month_sales[] = array(
                    "week" => $week,
                    "total_price" => 0,
                    "total_items" => 0
                );
                $week++;
            }
            $total_sales = 0;
            foreach ($result as $sales)
            {
                $week = $sales["purchase_week"];
                $current_month_sales[$week - 1]["week"] = $week;
                $current_month_sales[$week - 1]["total_price"] = number_format($sales["total_price"], 2);
                $current_month_sales[$week - 1]["total_items"] = $sales["total_items"];
                $total_sales += $sales["total_price"];
            }

            $info["current_month_sales"] = $current_month_sales;
            $info["total_sales"] = number_format($total_sales, 2);
        }

        $result = Db::execute("SELECT rp.`id`, rp.`name`, SUM(ri.`qty`) AS total_items FROM rewards_products AS rp
INNER JOIN `rewards_order_items` AS ri ON rp.id = ri.`product_id`
GROUP BY rp.`id` ORDER BY total_items DESC ");
//        $result = Db::db_execute(USE_SPHERE_DB,"SELECT sp.id,sp.name,SUM(soi.quantity) total_items FROM shop_orders AS so
//INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id
//INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id
//GROUP BY sp.id ORDER BY total_items DESC LIMIT 0, 10
//");
        $count = count($result);
        if ($count > 0)
        {
            $info["top_10_saleable_products"] = $result;
        }

        while ($count < 10)
        {
            $info["top_10_saleable_products"][] = array(
                "id" => "",
                "name" => "",
                "total_items" => ""
            );
            $count++;
        }

        $per_month_sales = array();
        $month_names = array(
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        );
        $count = 0;
        while ($count < 12)
        {
            $per_month_sales[] = array(
                "month_name" => $month_names[$count],
                "purchase_month" => "",
                "total_price" => "0",
                "total_items" => "0"
            );
            $count++;
        }

        $total_sales_per_month = 0;

        $result = Db::execute("SELECT MONTH(ro.purchased_on) AS purchase_month, SUM(ro.total_price) AS total_price, SUM(ro.total_items) AS total_items FROM rewards_orders AS ro WHERE YEAR(ro.purchased_on) = YEAR(CURRENT_DATE) GROUP BY purchase_month ORDER BY purchase_month");

//        $result = Db::db_execute(USE_SPHERE_DB,"SELECT SUM(soi.price*soi.quantity) AS total_price,SUM(soi.quantity) AS total_items,MONTH(so.order_datetime) AS purchase_month FROM shop_orders AS so
//INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id
//WHERE YEAR(so.order_datetime) = YEAR(CURRENT_DATE) GROUP BY purchase_month ORDER BY purchase_month");
        $month_data = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if (count($result) > 0)
        {
            foreach ($result as $r)
            {
                $total_sales_per_month += $r["total_price"];
                $month = $r["purchase_month"];
                $per_month_sales[$month - 1]["month_name"] = $month_names[$month - 1];
                $per_month_sales[$month - 1]["purchase_month"] = $r["purchase_month"];
                $per_month_sales[$month - 1]["total_price"] = number_format($r["total_price"], 2);
                $per_month_sales[$month - 1]["total_items"] = $r["total_items"];
                $month_data[] = $r["total_price"];
            }
        }

        self::createPerMonthChart($month_data);

        $info["per_month_sales"] = $per_month_sales;
        $info["total_sales_per_month"] = number_format($total_sales_per_month, 2);

        $total_sales_per_year = 0;

        $result = Db::execute("SELECT YEAR(ro.purchased_on) AS purchase_year, SUM(ro.total_price) AS total_price, SUM(ro.total_items) AS total_items FROM rewards_orders AS ro GROUP BY purchase_year ORDER BY purchase_year");
//        $result = Db::db_execute(USE_SPHERE_DB,"SELECT YEAR(so.order_datetime) AS purchase_year, SUM(so.total) AS total_price, SUM(soi.quantity) AS total_items FROM shop_orders AS so
//INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id GROUP BY purchase_year ORDER BY purchase_year");
        if (count($result) > 0)
        {
            foreach ($result as $r)
            {
                $total_sales_per_year += $r["total_price"];
            }
            $info["per_year_sales"] = $result;
        }
        $info["total_sales_per_year"] = number_format($total_sales_per_year, 2);

        self::displayGraph();

        GUI::render("admin/rewards_reports.tpl.php", $info);
    }

    private function createPerMonthChart($ydata)
    {
        require_once 'lib/jpgraph/jpgraph.php';
        require_once 'lib/jpgraph/jpgraph_line.php';

        $width = 780;
        $height = 250;

        $graph = new Graph($width, $height);
        $graph->SetScale('textlin');

        $graph->SetMargin(40, 20, 20, 40);
        $graph->title->Set('Sales per Month');
        $graph->xaxis->title->Set('Month');

        $lineplot = new LinePlot($ydata);

        $graph->Add($lineplot);
        unlink('images/current-sales-per-month.png');
        $graph->Stroke('images/current-sales-per-month.png');
    }

    public function getOrderList()
    {
        $result = Db::query(Table::ORDERS, array("*"), null, "0,10");
        return $result;
    }

    private function displayGraph()
    {
//        require_once 'lib/phpChart_Lite/conf.php';
    }

    public function getMonths()
    {
        $months = array(
            0 => array("id" => 1, "name" => "Jan"),
            1 => array("id" => 2, "name" => "Feb"),
            2 => array("id" => 3, "name" => "Mar"),
            3 => array("id" => 4, "name" => "Apr"),
            4 => array("id" => 5, "name" => "May"),
            5 => array("id" => 6, "name" => "Jun"),
            6 => array("id" => 7, "name" => "Jul"),
            7 => array("id" => 8, "name" => "Aug"),
            8 => array("id" => 9, "name" => "Sep"),
            9 => array("id" => 10, "name" => "Oct"),
            10 => array("id" => 11, "name" => "Nov"),
            11 => array("id" => 12, "name" => "Dec")
        );

        return $months;
    }

    public function getYears()
    {
        $years = array();
        $year = 2013;
        while ($year < date("Y") + 1)
        {
            $years[] = $year;
            $year++;
        }
        return $years;
    }

    public function getDays()
    {
        $days = array();
        $day = 1;
        while ($day <= 31)
        {
            $days[] = $day;
            $day++;
        }
        return $days;
    }

}

class MemberListReportPDF extends FPDF
{

    function Header()
    {
        $this->Image('images/sphere/mlm-logo.png', 10, 6, 30);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(80);
        $this->Cell(30, 10, 'LifeSense Member List', 0, 1, 'C');
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

class ProductListReportPDF extends FPDF
{

    function Header()
    {
        $this->Image('images/sphere/mlm-logo.png', 10, 6, 30);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(80);
        $this->Cell(30, 10, 'LifeSense Product List', 0, 1, 'C');
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

class SalesReportPDF extends FPDF
{

    function Header()
    {
        $this->Image('images/sphere/mlm-logo.png', 10, 6, 30);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(80);
        $this->Cell(30, 10, 'LifeSense Sales Report', 0, 1, 'C');
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

class OrderReportPDF extends FPDF
{

    function Header()
    {
        $this->Image('images/sphere/mlm-logo.png', 10, 6, 30);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(80);
        $this->Cell(30, 10, 'LifeSense Order Report', 0, 1, 'C');
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

?>