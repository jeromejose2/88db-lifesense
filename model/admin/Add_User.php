<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Add_User extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            Db::insert(Table::USERS,array("user" => Db::quote(parent::getPost("user")),
                                         "pass" => Db::quote(User::encodePass(parent::getPost("user"),parent::getPost("pass"))),
                                         "role" => parent::getPost("role"),
                                         "fullname" => Db::quote(parent::getPost("fullname")),
                                         "email" => Db::quote(parent::getPost("email")),
                                         "created_by" => Session::get("role"),
                                         "status" => User::STATUS_ACTIVE,
                                         "creation_date" => "NOW()"));
            parent::redirectTo("add-user-success");
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>