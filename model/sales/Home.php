<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Home extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_SALES)) {
            $info = array(
                "pageTitle" => "Home",
                "root" => ROOT,
                "name" => "",
                "last_login" => ""
            );
            
            $result = Db::execute("SELECT id,fullname FROM rewards_users WHERE id='$user_id' LIMIT 0,1");
            if (count($result) > 0) {
                $result = $result[0];                      
                $info["name"] = $result["fullname"];
            }
            
            $login_result = Db::execute("SELECT DATE_FORMAT(login_date,'%W, %M %e, %Y @ %h:%i %p') AS lastlogin FROM rewards_login_history LIMIT 0,2");
            $info["last_login"] = date("F j, Y, g:i a");
            if (count($login_result) > 1) {
                $info["last_login"] = $login_result[1]["lastlogin"];
            }            
            
            GUI::render("sales/home.tpl.php",$info);            
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
          
    private function getSponsor($sponsor) {
        $result = Db::query(Table::MEMBERS,array("firstname","lastname"),array("id" => $sponsor),"0,1");
        if (count($result) > 0) {
            return $result[0]["firstname"] . " ". $result[0]["lastname"];
        } else {
            return "";
        }
    }     
 }
?>