<?php
 require_once 'core/Model.php';
 
 class Shop extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        $info = array("pageTitle" => "Product List",
                      "root" => ROOT);
        
        Session::start();
        $member_id = Session::get("member_id");
        
        $result = Db::query(Table::PRODUCTS,array("id","name","price","description","photo"),null,"0,10");
        $products = array();
        if (count($result) > 0) {
            foreach ($result as $product) {
                $products[] = array(
                    "id" => $product["id"],
                    "photo" => $product["photo"],
                    "name" => $product["name"],
                    "price" => $product["price"],
                    "description" => $product["description"]
                );
            }
        }
        
        $result = Db::query(Table::PRODUCT_TYPES,array("title"),array("id" => "1"),"0,10");
        if (count($result) > 0) {
            $info["category"] = $result[0]["title"];
        }
        
        $info["products"] = $products;
        $info["show_cart_items"] = 1;
        
        Session::register("member_id",$member_id);
        
		GUI::render("shop.tpl.php",$info);
    }
 }
?>