<?php
 require_once 'core/Model.php';
 
 class Resend extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        Session::start();
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            $result = Db::execute("SELECT rm.`id`,CONCAT(rm.`firstname`,rm.`lastname`) AS `fullname`,rm.`email`,ra.`activation_code` FROM rewards_members AS rm INNER JOIN rewards_users AS ru ON rm.`id` = ru.user 
INNER JOIN rewards_user_activation_codes AS ra ON rm.`id` = ra.`member_id` WHERE ru.`id` = '$user_id'");
            $code = "";
            if (count($result) > 0) {
                $result = $result[0];
                $code = $result["activation_code"];
                submitEmail($result["email"],$result["fullname"],$result["activation_code"]);
            }
            self::displayResendMessage(ROOT."verify/ac/".$code);
            
        } else {
            parent::redirectTo(ROOT . "home");
        }
	}
     
    public function displayResendMessage($link) {
        $info = array("pageTitle" => "Re-send Activation Code");
        $info["link"] = $link;
        GUI::render("resend.tpl.php",$info);
    }

    public function submitEmail($email,$fullname,$code) {
        $to      = $email;
        $subject = "LifeSense Activation Code";
        $message = "Hi " . $fullname . "\r\n";
        $message.= "Activate your account by clicking the link below:\r\n";
        $message.= ROOT . "verify/ac/" . $code ."\r\n";
        $headers = 'From: LifeSense <webmaster@lifesense.ph>' . "\r\n" .
                   'Reply-To: webmail@lifesense.ph' . "\r\n";
        mail($to,$subject,$message,$headers);
    }     
 }
?>