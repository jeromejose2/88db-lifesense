<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class EditAccount extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            if (parent::isPostRequest())
            {
                $firstname = Db::quote(parent::getPost("firstname"));
                $lastname = Db::quote(parent::getPost("lastname"));
                $mi = Db::quote(parent::getPost("mi"));
                $email = Db::quote(parent::getPost("email"));
                $phone_no = Db::quote(parent::getPost("phone_no"));
                $mobile_no = Db::quote(parent::getPost("mobile_no"));
                $street = Db::quote(parent::getPost("street"));
                $city = Db::quote(parent::getPost("city"));
                $state = Db::quote(parent::getPost("state"));
                $image = parent::getFile("file");
                $imagePath = self::uploadImage($image);

                if (isset($imagePath))
                {
                    $result = Db::execute("SELECT user FROM rewards_users WHERE id = '$user_id'");
                    if (count($result) > 0)
                    {
                        $member_id = $result[0]["user"];
                        $query = "UPDATE rewards_members 
                        SET firstname=$firstname,lastname=$lastname,
                        mi=$mi,email=$email,phone_no=$phone_no,
                        mobile_no=$mobile_no,street=$street,
                        city=$city,state=$state,
                        image_directory='$imagePath' 
                        WHERE id='$member_id'";
                        echo $query;

                        Db::execute($query);
                    }

                    self::displayUpdateResult('Information have been Successfully updated');
                }
                else
                {
                    self::displayUpdateResult('Information have been Unsuccessfully updated');
                }
            }
            self::displayEditForm($user_id);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

    public function uploadImage($image)
    {
        $imageDirectory = "C:/xampp/htdocs/lifesense/images/userimage/";
        $ext = pathinfo($image['name'], PATHINFO_EXTENSION);

        $allowed = array('jpg', 'gif', 'png');

        if ($image['tmp_name'] != "" && in_array($ext, $allowed) == true)
        {
            $user_id = Session::get("user_id");
            $image['name'] = $user_id . '.' . $ext;
            $imagePath = $imageDirectory . $user_id . '.' . $ext;
            move_uploaded_file($image['tmp_name'], $imagePath);
            $dbInsert = "images/userimage/" . $image['name'];
            return $dbInsert;
        }
        else
        {
            echo "Error in uploading";
            return null;
        }
    }

    public function displayUpdateResult($msg)
    {
        $info = array("pageTitle" => "Updated");
        $info["message"] = $msg;
        GUI::render("member/edit_account_result.tpl.php", $info);
    }

    public function displayEditForm($user_id)
    {
        $info = array("pageTitle" => "Edit Account Information");
        $result = Db::execute("SELECT rm.* FROM rewards_members AS rm INNER JOIN rewards_users AS ru ON rm.`id` = ru.`user`
            WHERE ru.`id` = '$user_id' LIMIT 0,1");
        if (count($result) > 0)
        {
            $result = $result[0];
            foreach ($result as $key => $value)
            {
                $info[$key] = $value;
            }
        }
        GUI::render("member/edit_account.tpl.php", $info);
    }

}

?>