<?php

require_once 'core/Model.php';

class Shop extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        $info = array("pageTitle" => "Product List",
            "root" => ROOT);

        Session::start();
        $user_id = Session::get("user_id");

        $products = array();
        $extras = parent::getExtras();
        $cat = 0;
        $supressDisplay = false;
        if ($extras != null && count($extras) > 0)
        {
            if (isset($extras["cat"]))
            {
                $cat = intval($extras["cat"]);
            }

            if (isset($extras["action"]))
            {
                if ($extras["action"] == "view")
                {
                    $supressDisplay = true;
                    self::displayAddItem($user_id, intval($extras["id"]));
                }
                else if ($extras["action"] == "add")
                {
                    $quantity = 0;
                    $price = 0;
                    $discount = 0;

                    if (parent::isPostRequest())
                    {
                        $quantity = parent::getPost("qty");
                        $price = parent::getPost("price");
                        $discount = parent::getPost("discount");
                        self::addItem($user_id, intval($extras["id"]), $quantity, $price, $discount);
                        $supressDisplay = true;
                        parent::redirectTo(ROOT . "members/shop");
                    }
                }
                else if ($extras["action"] == "viewcart")
                {
                    self::displayCart($user_id);
                    $supressDisplay = true;
                }
                else if ($extras["action"] == "checkout")
                {
                    if (parent::isPostRequest())
                    {
                        $orderid = self::processOrder($user_id);
                        parent::redirectTo(ROOT . "members/shop/action/invoice/id/$orderid");
                    }
                    else
                    {
                        self::displayCheckout();
                    }
                    $supressDisplay = true;
                }
                else if ($extras["action"] == "invoice")
                {
                    self::displayInvoice($user_id, intval($extras["id"]));
                    $supressDisplay = true;
                }
                elseif ($extras["action"] == 'AddItemShop')
                {
                    $supressDisplay = true;
                    if (parent::isPostRequest())
                    {
                        $itemcategory = parent::getPost("shopcategoryid");
                        $itemname = parent::getPost("itemname");
                        $itemdesc = parent::getPost("itemdesc");
                        $itemprize = parent::getPost("itemprize");
                        $itemstock = parent::getPost("itemstock");
                        $image = parent::getFile("file");
                        $imagePath = self::uploadImage($image);

                        Db::execute("INSERT INTO `rewards_products` (in_stock,product_type,price,description,name,tax,discount,product_code,photo) VALUES ('$itemstock','$itemcategory','$itemprize','$itemdesc','$itemname',0,0,0,'$imagePath')");

                        parent::redirectTo(ROOT . "members/shop");
                    }
                    else
                    {
                        $result = Db::execute("SELECT rc.`id`, rc.`title` FROM `rewards_product_catalog` AS rc ORDER BY rc.`title`");
                        $info = array("pageTitle" => "Add Item in Shop",
                            "rsUser" => $result,
                            "");
                        GUI::render("member/shop_additemshop.php", $info);
                    }
                }
            }
        }

        if (!$supressDisplay)
        {
            if ($cat > 0)
            {
                $products = Db::execute("SELECT rp.`id`, rp.`name`, rp.`price`, rp.`description`, rp.`photo` FROM `rewards_products` AS rp
    INNER JOIN `rewards_product_catalog` AS pc ON rp.`product_type` = pc.`id` WHERE pc.`id` = '$cat' LIMIT 0, 10");
            }
            else
            {
                $products = Db::execute("SELECT rp.`id`, rp.`name`, rp.`price`, rp.`description`, rp.`photo` FROM `rewards_products` AS rp");
            }

            $category_list = self::getCategories();
            if (count($category_list) > 0)
            {
                $info["category"] = $category_list[0]["title"];
            }

            $info["products"] = $products;
            $info["show_cart_items"] = 1;

            $result = Db::execute("SELECT rc.`id`, rc.`title` FROM `rewards_product_catalog` AS rc ORDER BY rc.`title`");
            $info["category_list"] = $result;

            $info["shop_cart_items"] = self::countShopCartItems($user_id);
            if (User::hasRole(User::USER_ROLE_ADMIN))
            {
                $info["admin_user"] = true;
            }

            GUI::render("member/shop.tpl.php", $info);
        }
    }

    public function uploadImage($image)
    {
        $coredir = dirname(__FILE__) . "/";
        $basepath = realpath($coredir . "../../") . "/";

        $imageDirectory = $basepath . "images/products/";
        $ext = pathinfo($image['name'], PATHINFO_EXTENSION);

        $allowed = array('jpg', 'gif', 'png');

        if ($image['tmp_name'] != "" && in_array($ext, $allowed) == true)
        {
            $user_id = Session::get("user_id");
            $image['name'] = $user_id . '.' . $ext;
            $imagePath = $imageDirectory . $user_id . '.' . $ext;
            move_uploaded_file($image['tmp_name'], $imagePath);
            $dbInsert = "images/products/" . $image['name'];
            return $dbInsert;
        }
        else
        {
            echo "Error in uploading";
            return null;
        }
    }

    public function createInvoice($user_id, $order_id, $invoice)
    {
        $pdf = new InvoicePDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 12);

        $result = Db::execute("SELECT ro.`id`, CONCAT(ro.`firstname`, ' ', ro.`lastname`) as `customer_name`, DATE_FORMAT(ro.`purchased_on`,'%W, %M %e, %Y @ %h:%i %p') as date_purchased, ro.`discount`, ro.`total_items`, ro.`tax`, ro.`shipping_price`, ro.`total_price` FROM rewards_orders as ro
INNER JOIN rewards_members as rm ON ro.`member_id` = rm.`id`
INNER JOIN rewards_order_status as rs ON ro.`status` = rs.`id`
WHERE ro.`id` = '$order_id'");
        $order_no = 0;
        $customer_name = "No Name";
        $purchase_date = "";
        $discount = 0;
        $total = 0;
        $tax = 0;
        $shipping_price = 0;

        if (count($result) > 0)
        {
            $result = $result[0];
            $order_no = str_pad($result["id"], 11, "0", STR_PAD_LEFT);
            $customer_name = $result["customer_name"];
            $purchase_date = $result["date_purchased"];
            $discount = $result["discount"];
            $total = $result["total_price"];
            $tax = $result["tax"];
            $shipping_price = $result["shipping_price"];
        }

        $pdf->Cell(38, 7, "Invoice No.: ", 0, 0);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(24, 7, $invoice, 0, 0);
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(38, 7, "Customer Name: ", 0, 0);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(200, 7, $customer_name, 0, 0);
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(38, 7, "Purchase Date: ", 0, 0);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(120, 7, $purchase_date, 0, 0);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(24, 7, "Quantity", 1, 0, 'C');
        $pdf->Cell(144, 7, "Description", 1, 0, 'C');
        $pdf->Cell(20, 7, "Price", 1, 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);

        $result = Db::execute("SELECT ri.`price`, ri.`discount`, ri.`qty`,rp.`name` FROM `rewards_order_items` as ri 
INNER JOIN `rewards_products` AS rp ON ri.`product_id` = rp.`id`
WHERE ri.`order_id` = '$order_id'");

        if (count($result) > 0)
        {
            foreach ($result as $item)
            {
                $pdf->Cell(24, 7, $item["qty"], 0, 0, 'L');
                $pdf->Cell(144, 7, $item["name"], 0, 0, 'L');
                $pdf->Cell(20, 7, "P " . $item["price"], 0, 0, 'R');
                $pdf->Ln();
            }
        }

        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(24, 7, "Discount", 0, 0, 'L');
        $pdf->Cell(144, 7, "", 0, 0, 'L');
        $pdf->Cell(20, 7, "$discount", 0, 0, 'R');
        $pdf->Ln();
        $pdf->Cell(24, 7, "Shipping Fee", 0, 0, 'L');
        $pdf->Cell(144, 7, "", 0, 0, 'L');
        $pdf->Cell(20, 7, "$shipping_price", 0, 0, 'R');
        $pdf->Ln();
        $pdf->Cell(24, 7, "Tax", 0, 0, 'L');
        $pdf->Cell(144, 7, "", 0, 0, 'L');
        $pdf->Cell(20, 7, "$tax", 0, 0, 'R');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 18);
        $pdf->Cell(24 + 140, 7, "Total", 0, 0, 'L');
        $pdf->SetFont('Arial', '', 18);
        $pdf->Cell(24, 7, "P $total", 0, 0, 'R');

        $pdf->Output(getcwd() . "/invoice/$invoice.pdf", "F");
    }

    public function displayInvoice($user_id, $order_id)
    {
        $result = Db::execute("SELECT invoice FROM rewards_order_invoice WHERE order_id='$order_id'");
        if (count($result) > 0)
        {
            $invoice = $result[0]["invoice"];
            header("Content-type: application/pdf");

            print file_get_contents(getcwd() . "/invoice/$invoice.pdf");
        }
        else
        {
            print "No Invoice";
        }
    }

    public function processOrder($user_id)
    {
        $cart_items = array();
        $cart_items = self::getShopCartItems($user_id);
        $total = 0.0;
        $items = 0;
        $member_id = 0;
        $orderid = 0;
        $shipping_street = parent::getPost("shipping_street");
        $shipping_city = parent::getPost("shipping_city");
        $shipping_state = parent::getPost("shipping_state");
        $shipping_country = parent::getPost("shipping_country");

        $billing_street = parent::getPost("billing_street");
        $billing_city = parent::getPost("billing_city");
        $billing_state = parent::getPost("billing_state");
        $billing_country = parent::getPost("billing_country");

        $firstname = parent::getPost("firstname");
        $lastname = parent::getPost("lastname");

        $phone_no = parent::getPost("phone_no");
        $mobile_no = parent::getPost("mobile_no");
        $email = parent::getPost("email");
        if (count($cart_items) > 0)
        {
            foreach ($cart_items as $item)
            {
                $total += floatval($item["price"]);
                $items += intval($item["quantity"]);
            }
            $result = Db::execute("SELECT rm.`id` FROM `rewards_members` AS rm INNER JOIN `rewards_users` AS ru ON rm.`id` = ru.`user`
    WHERE rm.`id` = '$user_id'");
            if (count($result) > 0)
            {
                $member_id = $result[0]["id"];
                $orderid = Db::insert(Table::ORDERS, array("member_id" => $member_id,
                            "purchased_on" => "NOW()",
                            "total_items" => $items,
                            "total_price" => $total,
                            "discount" => "'0'",
                            "tax" => "'0'",
                            "shipping_price" => "'0'",
                            "status" => "'1'",
                            "shipping_street" => Db::quote($shipping_street),
                            "shipping_city" => Db::quote($shipping_city),
                            "shipping_state" => Db::quote($shipping_state),
                            "shipping_country" => Db::quote($shipping_country),
                            "billing_street" => Db::quote($billing_street),
                            "billing_city" => Db::quote($billing_city),
                            "billing_state" => Db::quote($billing_state),
                            "billing_country" => Db::quote($billing_country),
                            "firstname" => Db::quote($firstname),
                            "lastname" => Db::quote($lastname),
                            "phone_no" => Db::quote($phone_no),
                            "mobile_no" => Db::quote($mobile_no),
                            "email" => Db::quote($email)));

                $invoice = "INV" . str_pad($orderid, 11, "0", STR_PAD_LEFT);
                Db::execute("INSERT INTO rewards_order_invoice (order_id,invoice,invoice_date) VALUES('$orderid','$invoice',NOW())");


                foreach ($cart_items as $item)
                {
                    Db::insert(Table::ORDER_ITEMS, array("order_id" => $orderid, "product_id" => $item["product_id"], "price" => $item["price"], "discount" => $item["discount"], "qty" => $item["quantity"]));
                    $id = $item["id"];
                    Db::execute("UPDATE `rewards_shop_cart_items` SET checkedout = '1' WHERE id = '$id'");
                }

                self::createInvoice($user_id, $orderid, $invoice);
            }
        }
        return $orderid;
    }

    public function displayCheckout()
    {
        $info = array("pageTitle" => "Product List",
            "root" => ROOT);
        GUI::render("member/shop_checkout.tpl.php", $info);
    }

    public function displayCart($user_id)
    {
        $info = array("pageTitle" => "Product List",
            "root" => ROOT);
        $cart_items = array();
        $cart_items = self::getShopCartItems($user_id);
        $info["cart_items"] = $cart_items;
        $total = 0.0;
        foreach ($cart_items as $item)
        {
            $total += floatval($item["price"]);
        }
        $info["total"] = $total;
        GUI::render("member/shop_cart_items.tpl.php", $info);
    }

    public function addItem($user_id, $product_id, $quantity, $price, $discount)
    {
        $result = Db::execute("SELECT rm.`id` FROM `rewards_members` AS rm INNER JOIN `rewards_users` AS ru ON rm.`id` = ru.`user`
WHERE rm.`id` = '$user_id'");
        if (count($result) > 0)
        {
            $result = $result[0];
            Db::insert(Table::SHOP_CART_ITEMS, array("member_id" => $result["id"], "product_id" => $product_id, "quantity" => $quantity, "price" => $price, "discount" => $discount));
        }
    }

    public function displayAddItem($user_id, $product_id)
    {
        $info = array("pageTitle" => "Product List",
            "root" => ROOT);
        $result = Db::execute("SELECT rp.* FROM `rewards_products` AS rp WHERE rp.`id` = '$product_id'");
        if (count($result) > 0)
        {
            $qty = array();
            $in_stock = $result[0]["in_stock"];
            $n = 0;
            while ($n < $in_stock)
            {
                $qty[] = $n;
                $n++;
            }
            $result[0]["qty"] = $qty;
            $info["product"] = $result;
            $info["shop_cart_items"] = self::countShopCartItems($user_id);
            $info["show_cart_items"] = 1;
        }
        GUI::render("member/shop_add_item.tpl.php", $info);
    }

    public function getCategories()
    {
        $result = Db::query(Table::PRODUCT_TYPES, array("title"), array("id" => "1"), "0,10");
        return $result;
    }

    public function getShopCartItems($user_id)
    {
        $result = Db::execute("SELECT ri.`id`, ri.`quantity`,ri.`discount`,(ri.`price` * ri.`quantity`) as price, ri.`product_id`, rp.`name` FROM `rewards_shop_cart_items` AS ri INNER JOIN `rewards_members` AS rm ON ri.`member_id` = rm.`id` INNER JOIN `rewards_users` AS ru ON rm.`id` = ru.`user` INNER JOIN `rewards_products` AS rp ON ri.`product_id` = rp.`id` WHERE rm.`id` = '$user_id' AND ri.`checkedout` = '0'");
        return $result;
    }

    public function countShopCartItems($user_id)
    {
        $shop_cart_items = 0;
        $result = Db::execute("SELECT COUNT(ri.`id`) AS total FROM `rewards_shop_cart_items` AS ri INNER JOIN `rewards_members` AS rm ON ri.`member_id` = rm.`id` INNER JOIN `rewards_users` AS ru ON rm.`id` = ru.`user` WHERE rm.`id` = '$user_id' AND checkedout = '0'");
        if (count($result) > 0)
        {
            $shop_cart_items = $result[0]["total"];
        }
        return $shop_cart_items;
    }

}

class InvoicePDF extends FPDF
{

    function Header()
    {
        $this->Image('images/sphere/mlm-logo.png', 10, 6, 30);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(80);
        $this->Cell(30, 10, 'LifeSense Invoice', 0, 1, 'C');
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

?>