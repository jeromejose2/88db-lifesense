<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class compose extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();
        $user_id = Session::get("user_id");
        $extras = parent::getExtras();

        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {

            if ($extras != null && count($extras) > 0)
            {
                if (isset($extras['action']))
                {
                    if ($extras['action'] == 'post')
                    {
                        $message = mysql_real_escape_string(parent::getPost("message"));
                        $from = str_pad($user_id, 11, "0", STR_PAD_LEFT);

                        if ($extras["id"] == NULL)
                        {
                            $to = null;
                        }
                        else
                        {
                            $to = str_pad($extras["id"], 11, "0", STR_PAD_LEFT);
                        }

                        if ($to == "" || !isset($to) || $to == null || $to == '00000000000')
                        {
                            $newTo = mysql_real_escape_string(parent::getPost("to"));
                            $to = $newTo;
                        }

                        Db::insert(Table::MESSAGES, array("message" => "'$message'",
                            "fromx" => "'$from'",
                            "tox" => "'$to'"
                        ));

                        self::displayComposeResult("Success", "Message Sent successfully");
                    }
                }
                $result = Db::db_execute(0, "SELECT ru.fullname, rm.image_directory FROM rewards_users ru, rewards_members rm WHERE ru.id=rm.id AND ru.id = $extras[id]");
                $fullname = $result[0]['fullname'];
                $image_directory = $result[0]['image_directory'];
                $conversation = self::displayConversation($extras["id"]);

                $info = array("pageTitle" => "Compose Message",
                    "tox" => $extras['id'],
                    "fullname" => $fullname,
                    "image_directory" => $image_directory,
                    "rsConversation" => $conversation);
                ;

                GUI::render("member/Compose.tpl.php", $info);
            }
            else
            {
                $result = Db::db_execute(0, "SELECT  id, CONCAT(lastname,', ',firstname,' ',mi) as fullname , image_directory FROM prime_rewards_db.rewards_members order by fullname;");

                $info = array("pageTitle" => "Compose Message",
                    "rsUser" => $result,
                    "");

                GUI::render("member/Compose2.tpl.php", $info);
            }
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

    public function displayComposeResult($title, $message)
    {
        $info = array("pageTitle" => $title,
            "message" => $message);
        GUI::render("member/compose_result.tpl.php", $info);
    }

    public function displayConversation($user2)
    {
        //return conversation of sender and reciever
        $user_id = Session::get("user_id");
        $query = "SELECT 
			m.id
			,m.datex 
			,m.message AS message
			,CONCAT(u1.lastname, ', ', u1.firstname, ' ', u1.mi, '.') AS sender
			,u1.id AS senderID
			,CONCAT(u2.lastname, ', ', u2.firstname, ' ', u2.mi, '.') AS receiver
			,u2.id AS receiverID
			FROM rewards_messages m 
			INNER JOIN rewards_members u1 ON u1.id = m.fromx 
			INNER JOIN rewards_members u2 ON u2.id = m.tox
			WHERE (m.tox=$user_id or m.fromx=$user_id) and (m.tox=$user2 or m.fromx=$user2)";

        $result = Db::db_execute(0, $query);

        $rsConversation[] = array('speaker' => "",
            'message' => "",
            'date' => "");

        for ($i = 0; $i < count($result); $i++)
        {

            if ($result[$i]["senderID"] == $user_id)
            {
                $rsConversation[$i]["speaker"] = "Me";
            }
            else
            {
                $rsConversation[$i]["speaker"] = $result[$i]["sender"];
            }

            $rsConversation[$i]["message"] = $result[$i]["message"];
            $rsConversation[$i]["date"] = $result[$i]["datex"];
        }
        $temp[] = $rsConversation;
        //print_r($rsConversation);
        return $rsConversation;
    }

}

?>