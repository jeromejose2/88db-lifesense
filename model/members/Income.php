<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Income extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if ($user_id != null && $user_id != "")
        {
            $result = Db::query(Table::USERS, array("income"), array("user_id" => $user_id), "1");
            if (count($result) > 0)
            {
                $result = $result[0];

                $sub_result = Db::query(Table::DOWNLINES, array("id", "parent_id", "user_id", "date_created"), array("parent_id" => $user_id), "0,5");
                $downlines = array();
                if (count($sub_result) > 0)
                {
                    $index = 0;

                    foreach ($sub_result as $r)
                    {
                        $dwl = Db::query(Table::USERS, array("user_id", "CONCAT(UCASE(SUBSTRING(firstname, 1, 1)),LCASE(SUBSTRING(firstname,2)),' ',UCASE(SUBSTRING(lastname, 1, 1)),LCASE(SUBSTRING(lastname,2))) as member_name", "DATE_FORMAT(date_created,'%m/%d/%Y') as date_created", "sponsor_id"), array("user_id" => $r["user_id"]), "1");
                        if (count($dwl) > 0)
                        {
                            $dwl = $dwl[0];
                            $downlines[] = array(
                                "index" => ($index + 1),
                                "member_name" => $dwl["member_name"],
                                "user_id" => $dwl["user_id"],
                                "date_created" => $dwl["date_created"],
                                "sponsor_id" => $dwl["sponsor_id"]
                            );
                        }
                        $index++;
                    }
                }

                GUI::render("member_income.tpl.php", array('pageTitle' => 'Home',
                    'show_cart_items' => 'true',
                    'root' => '../',
                    'income' => number_format($result["income"]),
                    'downlines' => $downlines));
            }
            else
            {
                
            }
        }
        else
        {
            parent::redirectTo("../logout");
        }
    }

}

?>