<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Account extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if ($user_id != null && $user_id != "")
        {
            $result = Db::query(Table::USERS, array("user_id", "CONCAT(UCASE(SUBSTRING(firstname, 1, 1)),LCASE(SUBSTRING(firstname,2)),' ',UCASE(SUBSTRING(lastname, 1, 1)),LCASE(SUBSTRING(lastname,2))) as member_name", "address", "landline_no", "email", "photo"), array("user_id" => $user_id), "1");
            if (count($result) > 0)
            {
                $result = $result[0];
                GUI::render("member_account.tpl.php", array('pageTitle' => 'Home',
                    'show_cart_items' => 'true',
                    'root' => '../',
                    'member_name' => $result["member_name"],
                    'address' => $result["address"],
                    'landline_no' => $result["landline_no"],
                    'email' => $result["email"],
                    'photo' => ($result["photo"] == "" ? "default_photo.jpg" : $result["photo"])
                ));
            }
        }
        else
        {
            parent::redirectTo("../logout");
        }
    }

}

?>