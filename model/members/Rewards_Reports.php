<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Rewards_Reports extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            $extras = parent::getExtras();
            self::displayReport($user_id);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

//    public function displayReport($user_id)
//    {
//        $info = array("pageTitle" => "Rewards Reports");
//
//        GUI::render("member/rewards_reports.tpl.php", $info);
//    }

    /* Start
     * Added By Jerome.j 03-25-2014
     */
    public function getMonths()
    {
        $months = array(
            0 => array("id" => 1, "name" => "Jan"),
            1 => array("id" => 2, "name" => "Feb"),
            2 => array("id" => 3, "name" => "Mar"),
            3 => array("id" => 4, "name" => "Apr"),
            4 => array("id" => 5, "name" => "May"),
            5 => array("id" => 6, "name" => "Jun"),
            6 => array("id" => 7, "name" => "Jul"),
            7 => array("id" => 8, "name" => "Aug"),
            8 => array("id" => 9, "name" => "Sep"),
            9 => array("id" => 10, "name" => "Oct"),
            10 => array("id" => 11, "name" => "Nov"),
            11 => array("id" => 12, "name" => "Dec")
        );

        return $months;
    }

    public function getYears()
    {
        $years = array();
        $year = 2013;
        while ($year < date("Y") + 1)
        {
            $years[] = $year;
            $year++;
        }
        return $years;
    }

    public function getDays()
    {
        $days = array();
        $day = 1;
        while ($day <= 31)
        {
            $days[] = $day;
            $day++;
        }
        return $days;
    }

    public function getOrderList()
    {
        $result = Db::query(Table::ORDERS, array("*"), null, "0,10");
        return $result;
    }

    private function createPerMonthChart($ydata)
    {
        require_once 'lib/jpgraph/jpgraph.php';
        require_once 'lib/jpgraph/jpgraph_line.php';

        $width = 780;
        $height = 250;

        $graph = new Graph($width, $height);
        $graph->SetScale('textlin');

        $graph->SetMargin(40, 20, 20, 40);
        $graph->title->Set('Sales per Month');
        $graph->xaxis->title->Set('Month');

        $lineplot = new LinePlot($ydata);

        $graph->Add($lineplot);
        unlink('images/current-sales-per-month.png');
        $graph->Stroke('images/current-sales-per-month.png');
        chmod('images/current-sales-per-month.png', 0777);
    }

    private function displayGraph()
    {
        require_once 'lib/phpChart_Lite/conf.php';
    }

    public function displayReport($userid)
    {
        $info = array(
            "pageTitle" => "Rewards Reports",
            "root" => ROOT,
        );

        $info["num_pages"] = 1;
        $info["current_page"] = 1;

        $total_members = 0;
        $total_member_this_month = 0;
        $total_products = 0;

        $result = Db::query(Table::MEMBERS, array("COUNT(id) as total_members"), null, null);
        if (count($result) > 0)
        {
            $total_members = intval($result[0]["total_members"]);
        }

        $result = Db::execute("SELECT COUNT(id) as total_members FROM rewards_members WHERE YEAR(creation_date) = YEAR(CURRENT_DATE) AND MONTH(creation_date) = MONTH(CURRENT_DATE)");
        if (count($result) > 0)
        {
            $total_member_this_month = intval($result[0]["total_members"]);
        }

        $result = Db::query(Table::PRODUCTS, array("COUNT(id) as total_products"), null, null);
        if (count($result) > 0)
        {
            $total_products = intval($result[0]["total_products"]);
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_ytd FROM rewards_orders WHERE status='3' AND YEAR(purchased_on) = YEAR(CURRENT_DATE()) AND member_id = $userid");
        $total_sales_ytd = 0;
        if (count($result) > 0)
        {
            $total_sales_ytd = $result[0]["total_sales_ytd"] == "" ? 0 : $result[0]["total_sales_ytd"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_unpaid_ytd FROM rewards_orders WHERE status='1'");
        $total_sales_unpaid_ytd = 0;
        if (count($result) > 0)
        {
            $total_sales_unpaid_ytd = $result[0]["total_sales_unpaid_ytd"] == "" ? 0 : $result[0]["total_sales_unpaid_ytd"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_current FROM rewards_orders WHERE status='3' AND YEAR(purchased_on) = YEAR(CURRENT_DATE) AND MONTH(purchased_on) = MONTH(CURRENT_DATE)
                    AND member_id = $userid ");
        $total_sales_current = 0;
        if (count($result) > 0)
        {
            $total_sales_current = $result[0]["total_sales_current"] == "" ? 0 : $result[0]["total_sales_current"];
        }

        $result = Db::execute("SELECT SUM(total_price) AS total_sales_unpaid_current FROM rewards_orders WHERE status='1' AND YEAR(purchased_on) = YEAR(CURRENT_DATE) AND MONTH(purchased_on) = MONTH(CURRENT_DATE)");
        $total_sales_unpaid_current = 0;
        if (count($result) > 0)
        {
            $total_sales_unpaid_current = $result[0]["total_sales_unpaid_current"] == "" ? 0 : $result[0]["total_sales_unpaid_current"];
        }

        $info["total_sales_current"] = $total_sales_current;
        $info["total_sales_unpaid_current"] = $total_sales_unpaid_current;

        $info["total_sales_ytd"] = $total_sales_ytd;
        $info["total_sales_unpaid_ytd"] = $total_sales_unpaid_ytd;

        $info["total_members"] = $total_members;
        $info["total_member_this_month"] = $total_member_this_month;
        $info["total_products"] = $total_products;

        $info["days"] = self::getDays();
        $info["months"] = self::getMonths();
        $info["years"] = self::getYears();

        $info["orders"] = self::getOrderList();

//        $result = Db::execute("SELECT rm.id,CONCAT(rm.firstname,' ',rm.lastname) AS name,rc.points FROM rewards_members AS rm
//INNER JOIN rewards_member_commissions AS rc ON rm.id = rc.member_id
//WHERE rc.`commission_date` = YEAR(CURRENT_DATE) AND MONTH(rc.`commission_date`) = MONTH(CURRENT_DATE)
//ORDER BY rc.`points` DESC LIMIT 0,10");
        $result = Db::execute("SELECT rm.id,rm.points, rm.id userID, CONCAT(rm.lastname,', ',rm.firstname,' ', rm.mi) name, rm.image_directory image, rm.creation_date date
                    FROM rewards_members rm,rewards_downlines rd
                    WHERE rm.id=rd.child AND rd.parent=19 ORDER BY rm.points desc LIMIT 0,10");

        $count = count($result);
        if ($count > 0)
        {
            $info["top_10_earning_members"] = $result;
        }
        while ($count < 10)
        {
            $info["top_10_earning_members"][] = array(
                "id" => "",
                "name" => "",
                "points" => ""
            );
            $count++;
        }

        $info["current_month"] = date("F");

        $result = Db::execute("SELECT (WEEK(purchased_on,5) - WEEK(DATE_SUB(purchased_on, 
        INTERVAL DAYOFMONTH(purchased_on)-1 DAY),5)+1) AS purchase_week, SUM(total_price) AS total_price,
         SUM(total_items) AS total_items 
         FROM rewards_orders WHERE YEAR(purchased_on) = YEAR(CURRENT_DATE)
          AND MONTH(purchased_on) = MONTH(CURRENT_DATE) GROUP BY WEEK(purchased_on) 
          ORDER BY purchased_on ASC");

        if (count($result) > 0)
        {
            $current_month_sales = array();
            $week = 1;
            while ($week <= 5)
            {
                $current_month_sales[] = array(
                    "week" => $week,
                    "total_price" => 0,
                    "total_items" => 0
                );
                $week++;
            }
            $total_sales = 0;
            foreach ($result as $sales)
            {
                $week = $sales["purchase_week"];
                $current_month_sales[$week - 1]["week"] = $week;
                $current_month_sales[$week - 1]["total_price"] = number_format($sales["total_price"], 2);
                $current_month_sales[$week - 1]["total_items"] = $sales["total_items"];
                $total_sales += $sales["total_price"];
            }

            $info["current_month_sales"] = $current_month_sales;
            $info["total_sales"] = number_format($total_sales, 2);
        }

        $result = Db::execute("SELECT rp.`id`, rp.`name`, SUM(ri.`qty`) AS total_items FROM rewards_products AS rp
INNER JOIN `rewards_order_items` AS ri ON rp.id = ri.`product_id`
GROUP BY rp.`id` ORDER BY total_items DESC LIMIT 0,10");
        $count = count($result);
        if ($count > 0)
        {
            $info["top_10_saleable_products"] = $result;
        }

        while ($count < 10)
        {
            $info["top_10_saleable_products"][] = array(
                "id" => "",
                "name" => "",
                "total_items" => ""
            );
            $count++;
        }

        $per_month_sales = array();
        $month_names = array(
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        );
        $count = 0;
        while ($count < 12)
        {
            $per_month_sales[] = array(
                "month_name" => $month_names[$count],
                "purchase_month" => "",
                "total_price" => "0",
                "total_items" => "0"
            );
            $count++;
        }

        $total_sales_per_month = 0;

        $result = Db::execute("SELECT MONTH(ro.purchased_on) AS purchase_month, SUM(ro.total_price) AS total_price,
         SUM(ro.total_items) AS total_items FROM rewards_orders AS ro WHERE YEAR(ro.purchased_on) = YEAR(CURRENT_DATE) 
         AND ro.member_id = $userid GROUP BY purchase_month ORDER BY purchase_month");
        $month_data = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if (count($result) > 0)
        {
            foreach ($result as $r)
            {
                $total_sales_per_month += $r["total_price"];
                $month = $r["purchase_month"];
                $per_month_sales[$month - 1]["month_name"] = $month_names[$month - 1];
                $per_month_sales[$month - 1]["purchase_month"] = $r["purchase_month"];
                $per_month_sales[$month - 1]["total_price"] = number_format($r["total_price"], 2);
                $per_month_sales[$month - 1]["total_items"] = $r["total_items"];
                $month_data[] = $r["total_price"];
            }
        }

        self::createPerMonthChart($month_data);

        $info["per_month_sales"] = $per_month_sales;
        $info["total_sales_per_month"] = number_format($total_sales_per_month, 2);

        $total_sales_per_year = 0;

        $result = Db::execute("SELECT YEAR(ro.purchased_on) AS purchase_year, SUM(ro.total_price) AS total_price, 
        SUM(ro.total_items) AS total_items FROM rewards_orders AS ro WHERE ro.member_id = $userid  GROUP BY purchase_year ORDER BY purchase_year");
        if (count($result) > 0)
        {
            foreach ($result as $r)
            {
                $total_sales_per_year += $r["total_price"];
            }
            $info["per_year_sales"] = $result;
        }
        $info["total_sales_per_year"] = number_format($total_sales_per_year, 2);

//        self::displayGraph();

        GUI::render("member/rewards_reports.tpl.php", $info);
    }

    /* End
     * Added By Jerome.j 03-25-2014
     */
}

?>