<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class GenealogyDownLine extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	

	public function render() {
        Session::start();
       
	$myurl = $_SERVER['QUERY_STRING'];
	echo $myurl;
	$myArray = explode('=', $myurl);
	$value = $myArray[2];
	echo $value;
	

	  $input_id = $value;
	  // echo $input_id;
		
        $user_id = Session::get("user_id");
		
		
       if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            $info2 = array(
                "pageTitle" => "Home",
            "root" => ROOT,
            );

            $members2 = array();
            //echo $user_id;
            // $result = Db::query(Table::DOWNLINES,
            //             array("parent","child"),
            //             array("parent" => $user_id),"0,5");
            
            $result = Db::query(Table::DOWNLINES,
                        array("parent","child"),
                        array("parent" => $input_id),"0,5");
            //running source code
            $level = 1;            
            if (count($result) > 0) {
                foreach ($result as $item2) {
                    $sub_info=array(
                        "level" => $level,
                        "name" => "",
                        "date" => "",
                        "points" => "",
                        "status" => ""
                    );
                    $sub_result = Db::query(Table::MEMBERS,array("id,firstname","lastname","creation_date","points","image_directory"),
                                                            array("id" => $item2["child"]),"0,1");
                    if (count($sub_result) > 0) {
                        $i = $sub_result[0];
                        $sub_info["name"] = "(".$i["id"].") ". $i["firstname"] . " " . $i["lastname"];
                        $sub_info["date"] = $i["creation_date"];
						$sub_info["id"]=$i["id"];
                        $sub_info["image"] = str_replace("C:/xampp/htdocs/lifesense", "http://localhost/lifesense", $i["image_directory"]);

                        $commission = Db::execute("SELECT SUM(points) AS points FROM rewards_member_commissions WHERE member_id = '".$item2["child"]."'");
                        $sp = 0;
                        if (count($commission) > 0) {
                            $sp = $commission[0]["points"];
                        }
                        $sub_info["points"] = $sp > 0 ? $sp : '0' . " SP";
                    }
                    
                    $members2[] = $sub_info;

                    $level++;
                }
            }
            
            while ($level <= 5+1) {
                $members2[] = array(
                    "level" => "",
                    "name" => "",
                    "date" => "",
                    "points" => "",
                    "status" => ""
                );
                $level++;
            }
            
            $info2["members2"] = $members2;
            $info2["num_pages"] = 1;
            $info2["current_page"] = 1;
            
            GUI::render("member/genealogydownline.tpl.php",$info2);
        } else {
		  GUI::render("member/genealogydownline.tpl.php",$info2);
           // parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>