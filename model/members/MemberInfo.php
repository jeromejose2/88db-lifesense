<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';
require_once 'core/Pagination.php';

class MemberInfo extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            $info = array(
                "pageTitle" => "Members",
                "root" => ROOT
            );
            $extras = parent::getExtras();
            if ($extras != null && count($extras) > 0)
            {
                $result = Db::query(Table::MEMBERS, array("*"), array("id" => $extras["mid"]), "0,1");
                if (count($result) > 0)
                {
                    $info["member_info"] = $result;
                }
            }
            GUI::render("member/memberinfo.tpl.php", $info);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

}

?>