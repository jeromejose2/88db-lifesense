<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class MyShop extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        if ($user_id != null && $user_id != "")
        {
            $result = Db::query(Table::USERS, array("username"), array("user_id" => $user_id), "1");
            if (count($result) > 0)
            {
                $result = $result[0];
                $sub_result = Db::query(Table::MYSHOP, array("owner"), array("owner" => $result["username"]), "1");
                if (count($sub_result) > 0)
                {
                    $sub_result = $sub_result[0];
                    GUI::render("member_myshop.tpl.php", array('pageTitle' => 'Home',
                        'show_cart_items' => 'true',
                        'root' => '../',
                        'shop_url' => "../shop/" . $sub_result["owner"]
                    ));
                }
            }
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

}

?>