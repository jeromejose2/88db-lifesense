<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';
require_once 'core/Pagination.php';

class Transactions extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

//        $user_id = Session::get("user_id2");
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            $info = array("pageTitle" => "Transactions",
                "show_cart_items" => "false");

            $num_pages = 1;
            $num_items = 0;
            $page = 1;

            $extras = parent::getExtras();
            if (count($extras) > 0)
            {
                if (isset($extras["page"]))
                {
                    $page = intval($extras["page"]);
                    $page = $page < 1 ? 1 : $page;
                }
            }

            $limit = 10;
            $offset = $limit * ($page - 1);
            $item_count = 0;

            $result = Db::execute("SELECT * FROM rewards_members AS rm INNER JOIN rewards_users AS ru ON ru.user = rm.id WHERE ru.id = '$user_id'");
//            $result = Db::db_execute(USE_SPHERE_DB, "SELECT * FROM shop_customers a WHERE a.id = $user_id");
            if (count($result) > 0)
            {
                //$result = Db::db_execute(0, "SELECT COUNT(so.id) As num_items 
                //        FROM shop_orders AS so 
                //        INNER JOIN shop_customers AS scu ON scu.id = so.customer_id 
                //        INNER JOIN shop_order_items AS soi ON soi.shop_order_id = so.id 
                //        INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                //        INNER JOIN pages AS pp ON sp.page_id = pp.id WHERE so.customer_id = $user_id");
                $result = Db::db_execute(0, "SELECT  
                            COUNT(rm.id) AS num_items FROM rewards_orders ro INNER JOIN rewards_order_items roi ON ro.id = roi.order_id
                            INNER JOIN rewards_products rp ON rp.id = roi.product_id
                            INNER JOIN rewards_members rm ON ro.member_id = rm.id
                            WHERE rm.id = $user_id ORDER BY ro.id DESC");
                //$email = $result[0]["email"];
                // $result = Db::db_execute(USE_SPHERE_DB,"SELECT COUNT(so.id) As num_items 
                //         FROM shop_orders AS so 
                //         INNER JOIN shop_customers AS scu ON scu.id = so.customer_id 
                //         INNER JOIN shop_order_items AS soi ON soi.shop_order_id = so.id 
                //         INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                //         INNER JOIN pages AS pp ON sp.page_id = pp.id WHERE scu.email=" . Db::quote($email));
                if (count($result) > 0)
                {
                    $num_items = $result[0]["num_items"];
                    $info["num_items"] = $num_items;
                }
                // $result = Db::db_execute(USE_SPHERE_DB,
                //     "SELECT so.id,so.order_datetime,sp.name AS product, pp.label, FORMAT(soi.price,2) AS price, soi.quantity, FORMAT((soi.price * soi.quantity),2) AS total 
                //     FROM shop_orders AS so 
                //     INNER JOIN shop_customers AS scu ON scu.id = so.customer_id 
                //     INNER JOIN shop_order_items AS soi ON soi.shop_order_id = so.id 
                //     INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                //     INNER JOIN pages AS pp ON sp.page_id = pp.id 
                //     WHERE scu.email=" . Db::quote($email) . " LIMIT $offset,$limit"); 
                //$result = Db::db_execute(0, "SELECT so.id,so.order_datetime,sp.name AS product, pp.label, FORMAT(soi.price,2) AS price, soi.quantity, FORMAT((soi.price * soi.quantity),2) AS total 
                //   FROM shop_orders AS so 
                //   INNER JOIN shop_customers AS scu ON scu.id = so.customer_id 
                //   INNER JOIN shop_order_items AS soi ON soi.shop_order_id = so.id 
                //   INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                //   INNER JOIN pages AS pp ON sp.page_id = pp.id 
                //   WHERE so.customer_id= $user_id LIMIT $offset,$limit");

                $result = Db::db_execute(0, "SELECT  
                            ro.id,ro.purchased_on as order_datetime,rp.name as product,rp.description as label,FORMAT(roi.price,2) AS price,roi.qty as quantity,FORMAT((roi.price * roi.qty),2) AS total
                            FROM rewards_orders ro INNER JOIN rewards_order_items roi ON ro.id = roi.order_id
                            INNER JOIN rewards_products rp ON rp.id = roi.product_id
                            INNER JOIN rewards_members rm ON ro.member_id = rm.id
                            WHERE rm.id = $user_id ORDER BY ro.id DESC LIMIT $offset,$limit");

                if (count($result) > 0)
                {
                    $info["transactions"] = $result;
                    $item_count = count($result);
                }
            }

            while ($item_count < $limit)
            {
                $info["transactions"][] = array("id" => "", "order_datetime" => "", "product" => "", "label" => "", "price" => "", "quantity" => "", "total=" > "");
                $item_count++;
            }

            $pages = Pagination::calc($page, 5, $num_items, $limit);

            $info["num_pages"] = $pages["total"];
            $info["current_page"] = $page;
            $info["pages"] = $pages["pages"];
            $info["first"] = $pages["first"];
            $info["prev"] = $pages["prev"];
            $info["next"] = $pages["next"];
            $info["last"] = $pages["last"];
            GUI::render("member/transactions.tpl.php", $info);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
    }

}

?>