<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Commissions extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id");
        $user_id2 = Session::get("user_id2");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            $info = array(
                "pageTitle" => "Account Settings",
                "root" => ROOT,
            );

            $commission = 0;
            $start_date = "";
            $end_date = "";
            $result = Db::execute("SELECT DATE_FORMAT(rmc.start_date,'%b %e %Y') AS start_date,DATE_FORMAT(rmc.end_date,'%b %e %Y') AS end_date,rmc.income 
                                  FROM rewards_member_commissions AS rmc 
                                  INNER JOIN rewards_members AS rm ON rmc.member_id = rm.id 
                                  INNER JOIN rewards_users AS ru ON rm.id = ru.user 
                                  WHERE ru.id = '$user_id' LIMIT 0,1");
            if (count($result) > 0)
            {
                $result = $result[0];
                $commission = $result["income"];
                $start_date = $result["start_date"];
                $end_date = $result["end_date"];
            }
            $info["commission"] = $commission;
            $info["start_date"] = $start_date;
            $info["end_date"] = $end_date;

            $result = Db::execute("SELECT rmc.id,DATE_FORMAT(rmc.start_date,'%b %e %Y') AS start_date,DATE_FORMAT(rmc.end_date,'%b %e %Y') AS end_date,rmc.income 
                                    FROM rewards_member_commissions AS rmc 
                                    INNER JOIN rewards_members AS rm ON rmc.member_id = rm.id 
                                    INNER JOIN rewards_users AS ru ON rm.id = ru.user 
                                    WHERE ru.id = '$user_id' ORDER BY end_date desc");

            $commissions = array();
            $total_commission = 0;
            if (count($result) > 0)
            {
                $commissions = $result;

                foreach ($result as $r)
                {
                    $total_commission += $r["income"];
                }
            }

            //choi add
            $result = Db::db_execute(0, "SELECT so.id,so.order_datetime,sp.name AS product, pp.label, FORMAT(soi.price,2) AS price, soi.quantity, FORMAT((soi.price * soi.quantity),2) AS total 
                    FROM shop_orders AS so 
                    INNER JOIN shop_customers AS scu ON scu.id = so.customer_id 
                    INNER JOIN shop_order_items AS soi ON soi.shop_order_id = so.id 
                    INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                    INNER JOIN pages AS pp ON sp.page_id = pp.id 
                    WHERE so.customer_id= $user_id");
            //echo "here";
            $info['sphere_points'] = 0;
            foreach ($result as $row)
            {
                //echo $row['total'] . '</br>';
                //eplace({(',') : '' 
                $total = str_replace(",", "", $row['total']);
                $info['sphere_points'] += ($total / 5);
            }

            // echo $info['sphere_points'];
            if (count($result) > 0)
            {
                $info["transactions"] = $result;
                $item_count = count($result);
            }
            //
            $info["commissions"] = $commissions;
            $info["total_commission"] = number_format($total_commission, 2);


            GUI::render("member/commissions.tpl.php", $info);
        }
        else
        {
//		 GUI::render("member/commissions.tpl.php",$info);
            parent::redirectTo("../logout");
        }
    }

}

?>