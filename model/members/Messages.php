<?php
require_once 'core/Model.php';
require_once 'core/Gui.php';

class Messages extends Model {
	
    public function __construct() {
        parent::__construct($this);		
    }

    public function create($extras=null) {
       parent::setExtras($extras);		
   }

   public function render() {
    Session::start();
    $member= array();
        //  Session::register("user_id",$result["id"]);
    $u_id = Session::get("user_id");
    $info = array();
    if (User::isAuthenticated($u_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
        $info = array(
            "pageTitle" => "Messages",
            "last_login" => ""
            );

        $query= "SELECT max(rm.message) message, rm.fromx, rm.tox, max(rm.datex) as datex , ru.fullname, rmem.image_directory 
        FROM rewards_messages rm, rewards_users ru, rewards_members rmem 
        WHERE rm.fromx = ru.user AND rm.fromx=rmem.id AND rm.tox=$u_id
        group by rm.fromx order by datex desc";
        $result = Db::db_execute(0, $query);
        if (count($result) > 0) {

            $info["message"] = $result;


        }

        GUI::render("member/messages.tpl.php",$info);
    } else {
        GUI::render("member/messages.tpl.php",$info);
           // parent::redirectTo(ROOT . "logout");
    }
}
}
?>