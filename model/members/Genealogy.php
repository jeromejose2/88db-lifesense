<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';
require_once 'core/Pagination.php';

class Genealogy extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
//        Session::start();
//
//        // $get_id = parent::getID("get_id");
//        //	echo $get_id;
//
//
//        $user_id = Session::get("user_id");
//
//        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
//        {
//            $info = array(
//                "pageTitle" => "Genealogy",
//                "root" => ROOT,
//            );
//            $query = "SELECT rm.points, rm.id userID, CONCAT(rm.lastname,', ',rm.firstname,' ', rm.mi) name, rm.image_directory image, rm.creation_date date
//                    FROM rewards_members rm,rewards_downlines rd
//                    WHERE rm.id=rd.child AND rd.parent=$user_id";
//
//            $members = Db::db_execute(0, $query);
//
//            /* $sub_result = Db::query(Table::MEMBERS,array("id,firstname","lastname","creation_date","points","image_directory"),
//              array("id" => $item["child"]),"0,1");
//              //echo $user_id;
//              // $result = Db::query(Table::DOWNLINES,
//              //             array("parent","child"),
//              //             array("parent" => $user_id),"0,5");
//
//              /* $result = Db::query(Table::DOWNLINES,
//              array("parent","child"),
//              array("parent" => $user_id),"0,5");
//              //running source code
//              $level = 1;
//              if (count($result) > 0) {
//              foreach ($result as $item) {
//              $sub_info=array(
//              "level" => $level,
//              "name" => "",
//              "date" => "",
//              "points" => "",
//              "status" => "",
//              "userID" => ""
//              );
//              $sub_result = Db::query(Table::MEMBERS,array("id,firstname","lastname","creation_date","points","image_directory"),
//              array("id" => $item["child"]),"0,1");
//              if (count($sub_result) > 0) {
//              $i = $sub_result[0];
//              $sub_info["userID"] = $i["id"];
//              $sub_info["name"] = "(".$i["id"].") ". $i["firstname"] . " " . $i["lastname"];
//              $sub_info["date"] = $i["creation_date"];
//              $sub_info["image"] = str_replace("C:/xampp/htdocs/rewards", "", $i["image_directory"]);
//
//
//              $commission = Db::execute("SELECT SUM(points) AS points FROM rewards_member_commissions WHERE member_id = '".$item["child"]."'");
//              $sp = 0;
//              if (count($commission) > 0) {
//              $sp = $commission[0]["points"];
//              }
//              $sub_info["points"] = $sp > 0 ? $sp : '0' . " SP";
//              }
//
//              $members[] = $sub_info;
//
//              $level++;
//              }
//              }
//
//              while ($level <= 5+1) {
//              $members[] = array(
//              "level" => "",
//              "name" => "",
//              "date" => "",
//              "points" => "",
//              "status" => ""
//              );
//              $level++;
//              } */
//
//            $info["members"] = $members;
//            /* $info["num_pages"] = 1;
//              $info["current_page"] = 1; */
//
//            //print_r($info);
//            GUI::render("member/genealogy.tpl.php", $info);
//exit();

        Session::start();

        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER))
        {
            $info = array(
                "pageTitle" => "Members",
                "root" => ROOT
            );

            $query = "SELECT rm.points, rm.id userID, CONCAT(rm.lastname,', ',rm.firstname,' ', rm.mi) name, rm.image_directory image, rm.creation_date date
                    FROM rewards_members rm,rewards_downlines rd
                    WHERE rm.id=rd.child AND rd.parent=$user_id";

            $members = Db::db_execute(0, $query);

            $num_members = 0;
            $num_pages = 1;
//                $result = Db::query(Table::MEMBERS, array("COUNT(id) as num_members"), null, null);
            if (count($members) > 0)
            {
                $num_members = intval($result[0]["num_members"]);
            }

            $page = 1;

            $extras = parent::getExtras();
            if (count($extras) > 0)
            {
                if (isset($extras["page"]))
                {
                    $page = intval($extras["page"]);
                    $page = $page < 1 ? 1 : $page;
                }
            }

            $limit = 10;
            $offset = $limit * ($page - 1);

            $pages = Pagination::calc($page, 5, $num_members, $limit);

            $info["num_pages"] = $pages["total"];
            $info["current_page"] = $page;
            $info["pages"] = $pages["pages"];
            $info["first"] = $pages["first"];
            $info["prev"] = $pages["prev"];
            $info["next"] = $pages["next"];
            $info["last"] = $pages["last"];

            $members = array();

//                $result = Db::execute("SELECT id,firstname,lastname,sponsor,creation_date,status,points,referal_code FROM rewards_members LIMIT $offset,$limit");
            $result = Db::execute("SELECT rm.id,firstname,lastname,sponsor,creation_date,status,points,referal_code FROM rewards_members rm,rewards_downlines rd WHERE rm.id=rd.child AND rd.parent= $user_id LIMIT $offset,$limit");
            $itemCount = 0;
            foreach ($result as $item)
            {
                $commission = Db::execute("SELECT SUM(points) AS points FROM rewards_member_commissions WHERE member_id = '" . $item["id"] . "'");
                $sp = 0;
                if (count($commission) > 0)
                {
                    $sp = $commission[0]["points"] == "" ? "0" : $commission[0]["points"];
                }
                $members[] = array(
                    "id" => $item["id"],
                    "name" => $item["firstname"] . " " . $item["lastname"],
                    "sponsor" => self::getSponsor($item["sponsor"]),
                    "date" => $item["creation_date"],
                    "points" => "$sp SP",
                    "status" => User::getStatus($item["status"]),
                    "referal_code" => $item["id"]
                );
                $itemCount++;
            }

            while ($itemCount < 10)
            {
                $members[] = array(
                    "id" => "",
                    "name" => "",
                    "sponsor" => "",
                    "date" => "",
                    "points" => "",
                    "status" => "",
                    "referal_code" => ""
                );
                $itemCount++;
            }

            $info["members"] = $members;

            GUI::render("member/members.tpl.php", $info);
        }
        else
        {
            parent::redirectTo(ROOT . "logout");
        }
//        }
//        else
//        {
//
//            GUI::render("member/genealogy.tpl.php", $info);
//            //  parent::redirectTo(ROOT . "logout");
//        }
    }

    private function getSponsor($sponsor)
    {
        $result = Db::query(Table::MEMBERS, array("firstname", "lastname"), array("id" => $sponsor), "0,1");
        if (count($result) > 0)
        {
            return $result[0]["firstname"] . " " . $result[0]["lastname"];
        }
        else
        {
            return "None";
        }
    }

    public static function getStatus($status)
    {
        if ($status == User::STATUS_NEW)
        {
            return "New";
        }
        else if ($status == User::STATUS_ACTIVE)
        {
            return "Active";
        }
        else if ($status == User::STATUS_DEACTIVATED)
        {
            return "Deactivated";
        }
        else if ($status == User::STATUS_BANNED)
        {
            return "Banned";
        }
        else if ($status == User::STATUS_CANCELLED)
        {
            return "Cancelled";
        }
        else
        {
            return "Unknown";
        }
    }

}

?>