<?php

require_once 'core/Model.php';

class SignIn extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        $username = parent::getPost("user");
        $password = parent::getPost("pass");

        $result = Db::db_execute(0, "SELECT * FROM rewards_users a WHERE a.email = '$username'");
        if (count($result) > 0)
        {
            Session::start();
//            Session::register("user_id2", $result[0]["id"]);
//            Session::register("role", 4);
            $user_id = $result[0]["id"];
            Session::register("user_id", $user_id);

            Db::update(Table::USERS, array("login_attempt_count" => "0"), array("id" => $result[0]["id"]));
            Db::execute("INSERT INTO rewards_login_history(login_date,user_id) VALUES(NOW(),'$user_id')");
//            parent::redirectTo(ROOT . "members/home");
        }
        else
        {
            parent::redirectTo(ROOT . "home");
        }
        $result = Db::query(Table::USERS, array("id", "user", "pass", "role", "status", "login_attempt_count", "notify_password_change"), array("email" => "'" . $username . "'"), "0,1");


        if (count($result) > 0)
        {
            $result = $result[0];

            if ($result["notify_password_change"] == 1)
            {
                
            }

            if (intval($result["login_attempt_count"]) > 4)
            {

                parent::redirectTo(ROOT . "home");
            }
            else
            {
                $test = User::encodePass($username, $password);
                if ($result["pass"] == $test)
                {

                    if ($result["role"] == User::USER_ROLE_ADMIN || $result["role"] == User::USER_ROLE_MEMBER || $result["role"] == User::USER_ROLE_MANAGER || $result["role"] == User::USER_ROLE_SALES)
                    {
                        if ($result["status"] == User::STATUS_ACTIVE)
                        {

                            Session::start();
                            Session::register("user_id", $result["id"]);
                            Session::register("role", $result["role"]);

                            $user_id = $result["id"];

                            Db::update(Table::USERS, array("login_attempt_count" => "0"), array("id" => $result["id"]));
                            Db::execute("INSERT INTO rewards_login_history(login_date,user_id) VALUES(NOW(),'$user_id')");

                            $user_home = "home";
                            if ($result["role"] == User::USER_ROLE_ADMIN)
                            {
                                $user_home = "admin/home";
                            }
                            else if ($result["role"] == User::USER_ROLE_MANAGER)
                            {
                                $user_home = "manager/home";
                            }
                            else if ($result["role"] == User::USER_ROLE_SALES)
                            {
                                $user_home = "sales/home";
                            }
                            else if ($result["role"] == User::USER_ROLE_MEMBER)
                            {
                                $user_home = "members/home";
                            }
                            //echo "2";
                            parent::redirectTo(ROOT . $user_home);
                        }
                        else
                        {
                            // echo "here 2";
                            if ($result["role"] == User::USER_ROLE_MEMBER && $result["status"] == User::STATUS_NEW)
                            {
                                Session::start();
                                Session::register("user_id", $result["id"]);
                                Session::register("role", $result["role"]);
                                //echo "3";
                                parent::redirectTo(ROOT . "activate");
                            }
                            else
                            {
                                //echo "4";
                                parent::redirectTo(ROOT . "home");
                            }
                        }
                    }
                    else
                    {
                        //echo "5";
                        parent::redirectTo(ROOT . "home");
                    }
                }
                else
                {



                    Db::update(Table::USERS, array("login_attempt_count" => "(login_attempt_count + 1)"), array("id" => $result["id"]));
                    var_dump($test);exit;
                    parent::redirectTo(ROOT . "home");
                }
            }
        }
        else
        {
            parent::redirectTo(ROOT . "home");
        }
//        echo "asdf" . Session::get("user_id");
    }

}

?>