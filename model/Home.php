<?php

require_once 'core/Model.php';
require_once 'core/Gui.php';

class Home extends Model
{

    public function __construct()
    {
        parent::__construct($this);
    }

    public function create($extras = null)
    {
        parent::setExtras($extras);
    }

    public function render()
    {
        Session::start();

        $user_id = Session::get("user_id2");
        if (User::isAuthenticated($user_id))
        {
            $user_home = "home";
            if (User::hasRole(User::USER_ROLE_ADMIN))
            {
                $user_home = "admin/home";
            }
            else if (User::hasRole(User::USER_ROLE_MEMBER))
            {
                $user_home = "members/home";
            }
            else if (User::hasRole(User::USER_ROLE_MANAGER))
            {
                $user_home = "manager/home";
            }
            else if (User::hasRole(User::USER_ROLE_SALES))
            {
                $user_home = "sales/home";
            }
            parent::redirectTo(ROOT . $user_home);
        }
        else
        {
            GUI::render("home.tpl.php", array("pageTitle" => "LifeSense",
                "announce_title" => "Company Announcement Title",
                "announce_content" => "Company Announcement Content"));
        }
    }

}

?>